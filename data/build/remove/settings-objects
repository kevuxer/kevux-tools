# fss-0001
#
# Builds the remove library of the project as objects for use in tests.
#
# Modes:
#   - android:           Compile on an android system (using Termux; may need modification depending on the android system).
#   - clang:             Use CLang rather than the default, which is generally GCC.
#   - coverage:          Compile for building coverage.
#   - fanalyzer:         Compile using GCC's -fanalyzer compile time option.
#   - gcc:               Use GCC specific settings.
#   - gcc_13:            Use gcc version 13 or greater specific settings.
#   - individual:        Compile using per project (individual) libraries, does not handle thread or threadless cases.
#   - individual_thread: This is required when compiling in individual mode with "thread" mode.
#   - level:             Compile using per level libraries.
#   - monolithic:        Compile using per monolithic libraries.
#   - test:              Compile for a test, such as unit testing.
#   - thread:            Compile with thread support.
#   - threadless:        Compile without thread support.
#

build_name remove
stage remove

version_major 0
version_minor 5
version_micro 0
version_file micro
version_target minor

modes android clang coverage fanalyzer gcc gcc_13 individual individual_thread level monolithic test thread threadless
modes_default individual individual_thread test thread gcc

build_compiler gcc
build_compiler-clang clang
build_indexer ar
build_indexer_arguments rcs
build_language c

build_sources_object common.c common/define.c common/enumeration.c common/print.c common/string.c common/type.c convert.c operate.c preprocess.c print/error.c print/message.c print/simulate.c print/verbose.c print/warning.c remove.c signal.c thread.c

build_sources_headers common.h common/define.h common/enumeration.h common/print.h common/string.h common/type.h convert.h operate.h preprocess.h print/error.h print/message.h print/simulate.h print/verbose.h print/warning.h remove.h signal.h thread.h

build_sources_documentation man

build_script yes
build_shared yes
build_static no

path_headers program/kevux/tools/remove/main
path_sources sources/c/program/kevux/tools/remove/main

has_path_standard no
preserve_path_headers yes

search_exclusive yes
search_shared yes
search_static yes

environment PATH LD_LIBRARY_PATH
environment LANG LC_ALL LC_COLLATE LC_CTYPE LC_FASTMSG LC_MESSAGES LC_MONETARY LC_NUMERIC LC_TIME LOCPATH NLSPATH

#defines -D_di_libcap_
defines -D_libcap_legacy_only_
defines -D_use_timegm_
defines-android -D_di_f_thread_attribute_affinity_get_ -D_di_f_thread_attribute_affinity_set_ -D_di_f_thread_attribute_concurrency_get_ -D_di_f_thread_attribute_concurrency_set_ -D_di_f_thread_attribute_default_get_ -D_di_f_thread_attribute_default_set_ -D_di_f_thread_cancel_ -D_di_f_thread_cancel_state_set_ -D_di_f_thread_cancel_test_ -D_di_f_thread_join_try_ -D_di_f_thread_join_timed_ -D_pthread_mutex_prioceiling_unsupported_ -D_di_f_thread_semaphore_file_close_ -D_di_f_thread_semaphore_file_open_ -D_di_f_thread_semaphore_file_delete_ -D_di_f_thread_cancel_type_set_
defines-threadless -D_di_thread_support_
defines-thread -D_pthread_attr_unsupported_ -D_pthread_sigqueue_unsupported_

# This is needed for glibc and strptime() usage.
defines -D_GNU_SOURCE=1

flags -O0 -g -fdiagnostics-color=always -Wno-logical-not-parentheses -Wno-parentheses -Wno-missing-braces
flags -fstack-clash-protection -fno-delete-null-pointer-checks
flags -Wl,-z,nodlopen -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now
flags-android -Wno-implicit-function-declaration -Wl,-z,norelro
flags-clang -Wno-logical-op-parentheses
flags-coverage -O0 --coverage -fprofile-abs-path -fprofile-dir=build/coverage/
flags-fanalyzer -fanalyzer
flags-gcc_13 -fstrict-flex-arrays=3
flags-test -O0 -fstack-protector-strong -Wall
flags-thread -pthread

flags_library -fPIC
flags_object -fPIC
flags_program-android -fPIE -Wl,-z,relro
