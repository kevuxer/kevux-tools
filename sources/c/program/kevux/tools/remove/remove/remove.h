/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * This program provides the base include for the remove program.
 */
#ifndef _kt_remove_remove_remove_h
#define _kt_remove_remove_remove_h

// Remove includes.
#include <program/kevux/tools/remove/main/remove.h>
#include <program/kevux/tools/remove/remove/string.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_remove_remove_h
