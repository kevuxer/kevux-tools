#include "remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_remove_program_name_s_
  const f_string_static_t kt_remove_program_name_s = macro_f_string_static_t_initialize_1(KT_REMOVE_program_name_s, 0, KT_REMOVE_program_name_s_length);
  const f_string_static_t kt_remove_program_name_long_s = macro_f_string_static_t_initialize_1(KT_REMOVE_program_name_long_s, 0, KT_REMOVE_program_name_long_s_length);
#endif // _di_remove_program_name_s_

#ifdef __cplusplus
} // extern "C"
#endif
