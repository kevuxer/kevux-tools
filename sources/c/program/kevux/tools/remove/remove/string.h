/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common string structures for the remove program.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_remove_remove_string_h
#define _kt_remove_remove_string_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The program name.
 */
#ifndef _di_kt_remove_program_name_s_
  #define KT_REMOVE_program_name_s      "remove"
  #define KT_REMOVE_program_name_long_s "Remove"

  #define KT_REMOVE_program_name_s_length      6
  #define KT_REMOVE_program_name_long_s_length 6
#endif // _di_kt_remove_program_name_s_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_remove_string_h
