/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 */
#ifndef _kt_remove_main_operate_h
#define _kt_remove_main_operate_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Operate on a single file.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This alters main.setting.state.status:
 *     F_yes on success and file remove.
 *     F_no on success and file not removed.
 *     F_data_not on success but path is an empty string.
 *
 *     F_no (with error bit set) on file not removed due to failure.
 *
 *     Errors (with error bit) from: f_string_dynamic_append().
 *
 *     Errors (with error bit) from: kt_remove_operate_file_recurse().
 *     Errors (with error bit) from: kt_remove_operate_file_remove_delete().
 *     Errors (with error bit) from: kt_remove_operate_file_remove().
 *     Errors (with error bit) from: kt_remove_preprocess_file().
 * @param path
 *   The path to the file to operate on.
 *
 *   This should always be TRUE when calling from the top level.
 *   This should always be FALSE if calling from within a fl_directory_do() callback.
 *   This is because fl_directory_do() handles directory traversal and processing.
 *
 * @see f_string_dynamic_append()
 *
 * @see kt_remove_operate_file_recurse()
 * @see kt_remove_operate_file_remove_delete()
 * @see kt_remove_operate_file_remove()
 * @see kt_remove_preprocess_file()
 */
#ifndef _di_kt_remove_operate_file_
  extern void kt_remove_operate_file(kt_remove_main_t * const main, const f_string_static_t path);
#endif // _di_kt_remove_operate_file_

/**
 * Perform actual file removal for directory files.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This does not directly alter main.setting.state.status.
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 * @return
 *   F_yes on success and file remove.
 *   F_no on success and file not removed.
 *   F_data_not on success but path is an empty string.
 *
 *   F_no (with error bit) on failure and file is not to be removed or cannot be removed.
 *   F_recurse (with error bit) on max recursion depth reached.
 *
 *   Errors (with error bit) from: f_directory_recurse_do_delete()
 *   Errors (with error bit) from: fl_directory_do()
 *
 * @see f_directory_recurse_do_delete()
 * @see f_file_remove()
 * @see fl_directory_do()
 */
#ifndef _di_kt_remove_operate_file_recurse_
  extern f_status_t kt_remove_operate_file_recurse(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate);
#endif // _di_kt_remove_operate_file_recurse_

/**
 * Perform directory recurse for a single file operation action.
 *
 * @param recurse
 *   The directory recurse data.
 *
 *   Must not be NULL.
 * @param name
 *   The name of the file or directory the action is being performed on.
 *   Does not have the parent directory path.
 *   May be empty at the top level.
 * @param flag
 *   A flag representing the particular directory action being performed.
 *
 * @see f_directory_remove()
 * @see fl_directory_do()
 */
#ifndef _di_kt_remove_operate_file_recurse_action_
  extern void kt_remove_operate_file_recurse_action(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag);
#endif // _di_kt_remove_operate_file_recurse_action_

/**
 * Handle errors while performing directory recurse for a single file operation action.
 *
 * @param recurse
 *   The directory recurse data.
 *
 *   Must not be NULL.
 * @param name
 *   The name of the file or directory the action is being performed on.
 *   Does not have the parent directory path.
 *   May be empty at the top level.
 * @param flag
 *   A flag representing the particular directory action being performed.
 *
 * @see fl_directory_do()
 */
#ifndef _di_kt_remove_operate_file_recurse_handle_
  extern void kt_remove_operate_file_recurse_handle(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag);
#endif // _di_kt_remove_operate_file_recurse_handle_

/**
 * Perform actual file removal for non-directory files.
 *
 * This returns status rather than altering the main.setting.state.status so that it can safely be called within recursive functions.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This does not alter main.setting.state.status.
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 * @return
 *   F_no on success but file is not to be removed.
 *   F_yes on success and file is removed.
 *
 *   F_no (with error bit) on failure and file is not to be removed or cannot be removed.
 *
 *   Errors (with error bit) from: f_file_link_read().
 *   Errors (with error bit) from: f_file_remove().
 *
 * @see f_file_link_read()
 * @see f_file_remove()
 */
#ifndef _di_kt_remove_operate_file_remove_normal_
  extern f_status_t kt_remove_operate_file_remove(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate);
#endif // _di_kt_remove_operate_file_remove_normal_

/**
 * Check if a file should be skipped based on the memory.
 *
 * If memory is disabled, then this should always return F_okay.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This alters main.setting.state.status:
 *     F_okay on success.
 *     F_data_not on success but path is an empty string.
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 *   The kt_remove_flag_file_operate_processed_d is set if the path is found in the memory cache.
 *
 *   Must not be NULL.
 *
 * @see f_string_dynamic_append()
 */
#ifndef _di_kt_remove_operate_memory_check_
  extern void kt_remove_operate_memory_check(kt_remove_main_t * const main, const f_string_static_t path, uint16_t * const flag_operate);
#endif // _di_kt_remove_operate_memory_check_

/**
 * Add the path to the memory cache.
 *
 * The memory cache is only added when the remember flag (kt_remove_main_flag_remember_d) is set.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This alters main.setting.state.status:
 *     F_okay on success.
 *     F_data_not on success but path is an empty string.
 *
 *     Errors (with error bit) from: f_string_dynamic_append().
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 * @see f_string_dynamic_append()
 */
#ifndef _di_kt_remove_operate_memory_save_
  extern void kt_remove_operate_memory_save(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate);
#endif // _di_kt_remove_operate_memory_save_

/**
 * Determine whether or not a file shall be removed based on the given flag.
 *
 * @param flag
 *   The flag to use when determining the shall remove decision.
 *
 * @return
 *   F_true on shall remove.
 *   F_false otherwise.
 */
#ifndef _di_kt_remove_operate_shall_remove_
  extern f_status_t kt_remove_operate_shall_remove(const uint16_t flag);
#endif // _di_kt_remove_operate_shall_remove_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_main_operate_h
