/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common data structures.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_remove_main_common_h
#define _kt_remove_main_common_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Perform the standard program setting load process.
 *
 * This prints error messages as appropriate.
 *
 * @param arguments
 *   The parameters passed to the process (often referred to as command line arguments).
 * @param main
 *   The main program and settings data.
 *
 *   The setting.flag has kt_remove_flag_option_used_d forcibly cleared on the start of this function.
 *
 *   Must not be NULL.
 *
 *   This alters setting.status:
 *     F_okay on success.
 *     F_data_not on success but nothing was provided to operate with.
 *
 *     F_parameter (with error bit) on parameter error.
 *
 *     Errors (with error bit) from: f_console_parameter_process().
 *     Errors (with error bit) from: f_file_stream_open().
 *     Errors (with error bit) from: f_memory_array_increase_by().
 *
 * @see f_console_parameter_process()
 * @see f_file_stream_open()
 * @see f_memory_array_increase_by()
 */
#ifndef _di_kt_remove_setting_load_
  extern void kt_remove_setting_load(const f_console_arguments_t arguments, kt_remove_main_t * const main);
#endif // _di_kt_remove_setting_load_

/**
 * Perform the standard program setting load process, specifically for the UID and GID.
 *
 * This prints error messages as appropriate.
 *
 * @param main
 *   The main program and settings data.
 *
 *   The setting.flag has kt_remove_flag_option_used_d forcibly cleared on the start of this function.
 *
 *   Must not be NULL.
 *
 *   This alters setting.status:
 *     F_okay on success.
 *
 *     F_parameter (with error bit) on parameter error.
 *
 *     Errors (with error bit) from: f_memory_array_increase_by().
 *     Errors (with error bit) from: kt_remove_get_id().
 * @param parameter
 *   The console parameter, which should represent either the group parameter or the user parameter.
 *
 *   Must not be NULL.
 * @param ids
 *   The array of group IDs or user IDs.
 *
 *   Must not be NULL.
 * @param flag
 *   The flags to assign when the group or user is loaded.
 *
 * @see f_memory_array_increase_by()
 * @see kt_remove_get_id()
 */
#ifndef _di_kt_remove_setting_load_id_
  extern void kt_remove_setting_load_id(kt_remove_main_t * const main, f_console_parameter_t * const parameter, f_ids_t * const ids, const f_string_static_t name, const uint64_t flag);
#endif // _di_kt_remove_setting_load_id_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_main_common_h
