/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common type structures.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_remove_common_type_h
#define _kt_remove_common_type_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Pre-define the main type so it can be used in child classes.
 */
#ifndef _di_kt_remove_main_t_typedef_
  typedef struct kt_remove_main_t_ kt_remove_main_t;
#endif // _di_kt_remove_main_t_typedef_

/**
 * A processed Date parameter.
 *
 * The start is inclusive and the stop is exclusive just like with f_range_t.
 *
 * operation: The comparison operation.
 * type:      The date type.
 *
 * start_year:       The year in which the seconds is relative to for the start date.
 * start_second:     The entire date value in seconds for the date or the start of a date range.
 * start_nanosecond: The remaining nanosecond not represented in the seconds for the date or the start of a date range.
 *
 * stop_year:        The year in which the seconds is relative to for the stop date.
 * stop_second:      The entire date value in seconds for the stop of a date range (not used for non-range dates).
 * stop_nanosecond:  The remaining nanosecond not represented in the seconds for the stop of a date range (not used for non-range dates).
 */
#ifndef _di_kt_remove_date_t_
  typedef struct {
    uint8_t operation;
    uint8_t type;

    f_time_t start_year;
    f_time_t start_second;
    f_time_t start_nanosecond;

    f_time_t stop_year;
    f_time_t stop_second;
    f_time_t stop_nanosecond;
  } kt_remove_date_t;

  #define kt_remove_date_t_initialize { \
    0, \
    0, \
    0, \
    0, \
    0, \
    0, \
    0, \
    0, \
  }
#endif // _di_kt_remove_date_t_

/**
 * Date parameters.
 *
 * array: An array of data parameters.
 * size:  Total amount of allocated space.
 * used:  Total number of allocated spaces used.
 */
#ifndef _di_kt_remove_dates_t_
  typedef struct {
    kt_remove_date_t *array;

    f_number_unsigned_t size;
    f_number_unsigned_t used;
  } kt_remove_dates_t;

  #define kt_remove_dates_t_initialize { \
    0, \
    0, \
    0, \
  }

  #define macro_kt_remove_dates_t_clear(dates) \
    dates.array = 0; \
    dates.size = 0; \
    dates.used = 0;
#endif // _di_kt_remove_dates_t_

/**
 * A processed mode parameter.
 *
 * type: The mode type.
 * mode: The right side date value.
 */
#ifndef _di_kt_remove_mode_t_
  typedef struct {
    uint8_t type;
    mode_t mode;
  } kt_remove_mode_t;

  #define kt_remove_mode_t_initialize { \
    0, \
    0, \
  }
#endif // _di_kt_remove_mode_t_

/**
 * Mode parameters.
 *
 * array: An array of mode parameters.
 * size:  Total amount of allocated space.
 * used:  Total number of allocated spaces used.
 */
#ifndef _di_kt_remove_modes_t_
  typedef struct {
    kt_remove_mode_t *array;

    f_number_unsigned_t size;
    f_number_unsigned_t used;
  } kt_remove_modes_t;

  #define kt_remove_modes_t_initialize { \
    0, \
    0, \
    0, \
  }

  #define macro_kt_remove_modes_t_clear(dates) \
    dates.array = 0; \
    dates.size = 0; \
    dates.used = 0;
#endif // _di_kt_remove_modes_t_

/**
 * The program cache.
 *
 * buffer: The generic buffer.
 * files:  A collection of files, often used during path recursion like those associated with the tree parameter.
 * memory: A list of paths or partial paths representing files already processed.
 * tree:   A collection of files to process as a result of the --tree command.
 */
#ifndef _di_kt_remove_cache_t_
  typedef struct {
    f_string_dynamic_t buffer;
    f_string_dynamics_t files;
    f_string_dynamics_t memory;
    f_string_dynamics_t tree;
  } kt_remove_cache_t;

  #define kt_remove_cache_t_initialize \
    { \
      f_string_dynamic_t_initialize, \
      f_string_dynamics_t_initialize, \
      f_string_dynamics_t_initialize, \
      f_string_dynamics_t_initialize, \
    }
#endif // _di_kt_remove_cache_t_

/**
 * The main program callbacks.
 *
 * print_help:                    Print help.
 * process_normal:                Process normally (data from parameters and files).
 * process_operate_file:          Process an individual file, returning F_done to designate handled, and F_okay for letting parent continue handling.
 * process_operate_file_simulate: Simulate process of an individual file, returning F_done to designate handled, and F_okay for letting parent continue handling.
 */
#ifndef _di_kt_remove_callback_t_
  typedef f_status_t (*print_help_call_t)(fl_print_t * const print, const f_color_context_t context);
  typedef void (*process_normal_call_t)(kt_remove_main_t * const main);
  typedef void (*process_operate_file_call_t)(kt_remove_main_t * const main, const f_string_static_t path, const struct stat statistics, uint16_t * const flag);
  typedef void (*process_operate_file_simulate_call_t)(kt_remove_main_t * const main, const f_string_static_t path, const struct stat statistics, const uint16_t flag_operate, uint16_t * const flag);

  typedef struct {
    print_help_call_t print_help;
    process_normal_call_t process_normal;
    process_operate_file_call_t process_operate_file;
    process_operate_file_simulate_call_t process_operate_file_simulate;
  } kt_remove_callback_t;

  #define kt_remove_callback_t_initialize \
    { \
      0, \
      0, \
      0, \
      0, \
    }
#endif // _di_kt_remove_callback_t_

/**
 * The main program settings.
 *
 * This is passed to the program-specific main entry point to designate program settings.
 * These program settings are often processed from the program arguments (often called the command line arguments).
 *
 * flag: Flags passed to the main function.
 *
 * status_thread: A status used eclusively by the threaded signal handler.
 * state:         The state data used when processing data.
 *
 * program_name:      The short name of the program.
 * program_name_long: The human friendly name of the program.
 *
 * files: An array of file names (full paths to the files) to remove.
 *
 * accessed: An array of last accessed dates used for comparison.
 * changed:  An array of changed on dates used for comparison.
 * updated:  An array of last updated dates used for comparison.
 * modes:    An array of modes used for comparison.
 * groups:   An array of Group IDs.
 * users:    An array of User IDs.
 */
#ifndef _di_kt_remove_setting_t_
  typedef struct {
    uint64_t flag;

    f_status_t status_thread;
    f_state_t state;

    f_string_dynamics_t files;

    kt_remove_dates_t accessed;
    kt_remove_dates_t changed;
    kt_remove_dates_t updated;
    kt_remove_modes_t modes;
    f_gids_t groups;
    f_uids_t users;

    const f_string_static_t *program_name;
    const f_string_static_t *program_name_long;
  } kt_remove_setting_t;

  #define kt_remove_setting_t_initialize \
    { \
      kt_remove_main_flag_none_d, \
      F_okay, \
      macro_f_state_t_initialize_1(kt_remove_allocation_large_d, kt_remove_allocation_small_d, F_okay, 0, 0, &fll_program_standard_signal_handle, 0, 0, 0, 0), \
      f_string_dynamics_t_initialize, \
      kt_remove_dates_t_initialize, \
      kt_remove_dates_t_initialize, \
      kt_remove_dates_t_initialize, \
      kt_remove_modes_t_initialize, \
      f_gids_t_initialize, \
      f_uids_t_initialize, \
      0, \
      0, \
    }
#endif // _di_kt_remove_setting_t_

/**
 * The main program data as a single structure.
 *
 * cache:   The program cache.
 * call:    The program callbacks.
 * program: The main program data.
 * setting: The settings data.
 */
#ifndef _di_kt_remove_main_t_
  struct kt_remove_main_t_ {
    kt_remove_cache_t cache;
    kt_remove_callback_t call;
    fll_program_data_t program;
    kt_remove_setting_t setting;
  };

  #define kt_remove_main_t_initialize \
    { \
      kt_remove_cache_t_initialize, \
      kt_remove_callback_t_initialize, \
      fll_program_data_t_initialize, \
      kt_remove_setting_t_initialize, \
    }
#endif // _di_kt_remove_main_t_

/**
 * Delete the program main setting data.
 *
 * @param cache
 *   The program main cache data.
 *
 *   This does not alter setting.state.status.
 */
#ifndef _di_kt_remove_cache_delete_
  extern void kt_remove_cache_delete(kt_remove_cache_t * const cache);
#endif // _di_kt_remove_cache_delete_

/**
 * Deallocate main program data.
 *
 * @param setting_make
 *   The make setting data.
 *
 *   This does not alter main.setting.state.status.
 */
#ifndef _di_kt_remove_main_delete_
  extern void kt_remove_main_delete(kt_remove_main_t * const main);
#endif // _di_kt_remove_main_delete_

/**
 * Delete the program main setting data.
 *
 * @param setting
 *   The program main setting data.
 *
 *   This does not alter setting.state.status.
 */
#ifndef _di_kt_remove_setting_delete_
  extern void kt_remove_setting_delete(kt_remove_setting_t * const setting);
#endif // _di_kt_remove_setting_delete_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_common_type_h
