#include "../remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_remove_f_a_
  const f_string_t kt_remove_f_a[] = {
    "f_console_parameter_prioritize_right",
    "f_console_parameter_process",
    "f_file_mode_from_string",
    "f_file_mode_to_mode",
    "f_file_remove",
    "f_memory_array_increase",
    "f_memory_array_increase_by",
    "f_memory_array_resize",
    "f_string_dynamic_partial_append",
    "f_string_dynamic_seek_to_back",
    "f_thread_create",
    "f_utf_is_digit",
    "f_utf_is_whitespace",
    "fl_conversion_dynamic_partial_to_unsigned_detect",
    "fl_conversion_dynamic_to_unsigned_detect",
    "fl_path_clean",
    "fl_recurse_do",
    "fll_program_parameter_process_context",
    "fll_program_parameter_process_empty",
    "fll_program_parameter_process_verbosity",
    "kt_remove_convert_date_relative",
    "kt_remove_convert_timezone",
    "kt_remove_dates_resize",
    "kt_remove_get_id",
    "kt_remove_modes_resize",
    "kt_remove_operate_memory_save",
    "kt_remove_setting_load",
  };
#endif // _di_kt_remove_f_a_

#ifdef __cplusplus
} // extern "C"
#endif
