/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common enumeration types.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_remove_common_enumeration_h
#define _kt_remove_common_enumeration_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The main program parameters.
 */
#ifndef _di_kt_remove_parameter_e_
  enum {
    kt_remove_parameter_help_e,
    kt_remove_parameter_copyright_e,
    kt_remove_parameter_light_e,
    kt_remove_parameter_dark_e,
    kt_remove_parameter_no_color_e,
    kt_remove_parameter_verbosity_quiet_e,
    kt_remove_parameter_verbosity_error_e,
    kt_remove_parameter_verbosity_normal_e,
    kt_remove_parameter_verbosity_verbose_e,
    kt_remove_parameter_verbosity_debug_e,
    kt_remove_parameter_version_e,

    kt_remove_parameter_accessed_e,
    kt_remove_parameter_block_e,
    kt_remove_parameter_character_e,
    kt_remove_parameter_changed_e,
    kt_remove_parameter_different_e,
    kt_remove_parameter_directory_e,
    kt_remove_parameter_empty_e,
    kt_remove_parameter_fifo_e,
    kt_remove_parameter_follow_e,
    kt_remove_parameter_force_e,
    kt_remove_parameter_group_e,
    kt_remove_parameter_link_e,
    kt_remove_parameter_local_e,
    kt_remove_parameter_mode_e,
    kt_remove_parameter_prompt_e,
    kt_remove_parameter_recurse_e,
    kt_remove_parameter_regular_e,
    kt_remove_parameter_remember_e,
    kt_remove_parameter_same_e,
    kt_remove_parameter_simulate_e,
    kt_remove_parameter_socket_e,
    kt_remove_parameter_stay_e,
    kt_remove_parameter_tree_e,
    kt_remove_parameter_updated_e,
    kt_remove_parameter_user_e,
    kt_remove_parameter_utc_e,
  }; // enum

  #define kt_remove_console_parameter_t_initialize \
    { \
      macro_fll_program_console_parameter_standard_initialize, \
      \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_accessed_s,  kt_remove_long_accessed_s,  2, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_block_s,     kt_remove_long_block_s,     0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_character_s, kt_remove_long_character_s, 0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_changed_s,   kt_remove_long_changed_s,   2, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_different_s, kt_remove_long_different_s, 0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_directory_s, kt_remove_long_directory_s, 0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_empty_s,     kt_remove_long_empty_s,     1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_fifo_s,      kt_remove_long_fifo_s,      0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_follow_s,    kt_remove_long_follow_s,    0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_force_s,     kt_remove_long_force_s,     0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_group_s,     kt_remove_long_group_s,     1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_link_s,      kt_remove_long_link_s,      0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_5(                             kt_remove_long_local_s,     0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_mode_s,      kt_remove_long_mode_s,      2, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_prompt_s,    kt_remove_long_prompt_s,    1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_recurse_s,   kt_remove_long_recurse_s,   0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_regular_s,   kt_remove_long_regular_s,   0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_5(                             kt_remove_long_remember_s,  1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_same_s,      kt_remove_long_same_s,      0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_simulate_s,  kt_remove_long_simulate_s,  0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_socket_s,    kt_remove_long_socket_s,    0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_stay_s,      kt_remove_long_stay_s,      0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_tree_s,      kt_remove_long_tree_s,      0, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_updated_s,   kt_remove_long_updated_s,   2, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_remove_short_user_s,      kt_remove_long_user_s,      1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_5(                             kt_remove_long_utc_s,       0, f_console_flag_normal_e), \
    }

  #define kt_remove_total_parameters_d (f_console_parameter_state_type_total_d + 26)
#endif // _di_kt_remove_parameter_e_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_common_enumeration_h
