/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common define types.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_remove_common_define_h
#define _kt_remove_common_define_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The program defines.
 *
 * Leap Year:
 *   - If can be evenly divided by 4, then this is a leap year. (@fixme relocate or move "Leap Year" comments where appropriate.)
 *
 * kt_remove_depth_*_d:
 *   - max: The maximum recursion depth to perform when recursing into a directory.
 *
 * kt_remove_signal_*_d:
 *   - check:          When not using threads, this is how often to perform the check (lower numbers incur more kernel I/O).
 *   - check_failsafe: When using threads, how many consecutive failures to check signal before aborting (as a recursion failsafe).
 *
 * kt_remove_time_seconds_in_*_d:
 *   - day:        Number of seconds in a day.
 *   - hour:       Number of seconds in a hour.
 *   - minute:     Number of seconds in a minute.
 *   - nanosecond: Number of seconds in a nanosecond.
 *   - week:       Number of seconds in a week.
 *   - year:       Number of seconds in a year (does not include leap years).
 *
 * kt_remove_time_year_*_d:
 *   - unix_epoch: The year in which the UNIX Epoch starts.
 */
#ifndef _di_kt_remove_d_
  #define kt_remove_depth_max_d F_directory_max_recurse_depth_d

  #define kt_remove_signal_check_d          20000
  #define kt_remove_signal_check_failsafe_d 20000

  #define kt_remove_time_seconds_in_day_d        86400
  #define kt_remove_time_seconds_in_hour_d       3600
  #define kt_remove_time_seconds_in_minute_d     60
  #define kt_remove_time_seconds_in_nanosecond_d 1000000000
  #define kt_remove_time_seconds_in_week_d       604800
  #define kt_remove_time_seconds_in_year_d       31536000

  #define kt_remove_time_year_unix_epoch_d 1970
#endif // _di_kt_remove_d_

/**
 * The program allocation defines.
 *
 * control_allocation_*_d:
 *   - console: An allocation step used for small buffers specifically for console parameter.
 *   - large:   An allocation step used for buffers that are anticipated to have large buffers.
 *   - pipe:    A buffer size used for processing piped data.
 *   - small:   An allocation step used for buffers that are anticipated to have small buffers.
 */
#ifndef _di_kt_remove_allocation_d_
  #define kt_remove_allocation_console_d 4
  #define kt_remove_allocation_large_d   2048
  #define kt_remove_allocation_pipe_d    16384
  #define kt_remove_allocation_small_d   8
#endif // _di_kt_remove_allocation_d_

/**
 * A set of flags used internally in the convert process.
 *
 * These are generally used during parsing of Time and EpochTime strings.
 *
 * kt_remove_flag_convert_*_d:
 * - colon:        Either single or double colon.
 * - colon_single: Single colon detected.
 * - colon_double: Double colon detected.
 * - match:        Matched either part.
 * - match_first:  Matched first (left) part.
 * - match_second: Matched second (right) part.
 */
#ifndef _di_kt_remove_flag_convert_d_
  #define kt_remove_flag_convert_none_d         0x0
  #define kt_remove_flag_convert_colon_d        0x3
  #define kt_remove_flag_convert_colon_single_d 0x1
  #define kt_remove_flag_convert_colon_double_d 0x2
  #define kt_remove_flag_convert_match_d        0xc
  #define kt_remove_flag_convert_match_first_d  0x4
  #define kt_remove_flag_convert_match_second_d 0x8
#endif // _di_kt_remove_flag_convert_d_

/**
 * Flags associated with a datetime.
 *
 * kt_remove_flag_date_*_d:
 *   - none:       No flags set.
 *   - equal:      Perform equal to on date, '==' or 'equal'.
 *   - less:       Perform less than on date, '<' or 'less'.
 *   - less_equal: Perform less than or equal to on date, '<=' or 'less_equal'.
 *   - more:       Perform greater than on date, '>' or 'more'.
 *   - more_equal: Perform greater than or equal to on date, '>=' or 'more_equal'.
 *   - not:        Perform not equal to on date. '<>' or 'not'
 *
 *   - year:       Date has a year.
 *   - now:        Date is relative to 'now'.
 *   - string:     Date is processed via the string date functions (such as via strftime_r() or getdate_r()).
 *   - time:       Date is based off of Time format.
 *   - time_epoch: Date is based off of EpochTime format.
 *   - today:      Date is relative to 'today'.
 *   - tomorrow:   Date is relative to 'tomorrow'.
 *   - unix:       Date is based off of Unix Epoch format.
 *   - yesterday:  Date is relative to 'yesterday'.
 */
#ifndef _di_kt_remove_flag_date_d_
  #define kt_remove_flag_date_none_d       0x0

  // Used for comparisons.
  #define kt_remove_flag_date_equal_d      0x1
  #define kt_remove_flag_date_less_d       0x2
  #define kt_remove_flag_date_less_equal_d 0x4
  #define kt_remove_flag_date_more_d       0x8
  #define kt_remove_flag_date_more_equal_d 0x10
  #define kt_remove_flag_date_not_d        0x20

  // Used for processing and converting.
  #define kt_remove_flag_date_now_d        0x1
  #define kt_remove_flag_date_string_d     0x2
  #define kt_remove_flag_date_time_d       0x4
  #define kt_remove_flag_date_time_epoch_d 0x8
  #define kt_remove_flag_date_today_d      0x10
  #define kt_remove_flag_date_tomorrow_d   0x20
  #define kt_remove_flag_date_unix_d       0x40
  #define kt_remove_flag_date_yesterday_d  0x80
#endif // _di_kt_remove_flag_date_d_

/**
 * Flags associated with performing an operation on a file.
 *
 * kt_remove_flag_file_operate_*_d:
 *   - none:            No flags set.
 *   - child:           This is a child of a file for some other file operation process.
 *   - directory:       Is a directory.
 *   - empty:           Is an empty directory.
 *   - follow:          Follow the symbolic link.
 *   - link:            The file being operated on is a link or is a followed link.
 *   - parent:          This is a parent of a file for some other file operation process.
 *   - processed:       This path is already processed.
 *   - recurse:         Perform recursively (only on directories).
 *   - remove:          Perform remove.
 *   - remove_fail:     Cannot perform remove due to failure.
 *   - remove_not:      Do not remove, but not a failure.
 *   - remove_not_fail: Helper used to designate both remove_fail and remove_not being set.
 */
#ifndef _di_kt_remove_flag_file_operate_d_
  #define kt_remove_flag_file_operate_none_d            0x0
  #define kt_remove_flag_file_operate_child_d           0x1
  #define kt_remove_flag_file_operate_directory_d       0x2
  #define kt_remove_flag_file_operate_empty_d           0x4
  #define kt_remove_flag_file_operate_follow_d          0x8
  #define kt_remove_flag_file_operate_link_d            0x10
  #define kt_remove_flag_file_operate_parent_d          0x20
  #define kt_remove_flag_file_operate_processed_d       0x40
  #define kt_remove_flag_file_operate_recurse_d         0x80
  #define kt_remove_flag_file_operate_remove_d          0x100
  #define kt_remove_flag_file_operate_remove_fail_d     0x200
  #define kt_remove_flag_file_operate_remove_not_d      0x400
  #define kt_remove_flag_file_operate_remove_not_fail_d 0x800
#endif // _di_kt_remove_flag_file_operate_d_

/**
 * Flags associated with a mode.
 *
 * kt_remove_flag_mode_*_e:
 *   - none:      No flags set.
 *   - different: Remove by mode matching different parts ('~~' or 'different').
 *   - not:       Remove by not exact mode match ('<>' or 'not').
 *   - same:      Remove by exact mode match ('==' or 'same').
 *   - similar:   Remove by mode matching same parts ('~=' or 'similar').
 */
#ifndef _di_kt_remove_flag_mode_d_
  #define kt_remove_flag_mode_none_d      0x0
  #define kt_remove_flag_mode_different_d 0x1
  #define kt_remove_flag_mode_not_d       0x2
  #define kt_remove_flag_mode_same_d      0x4
  #define kt_remove_flag_mode_similar_d   0x8
#endif // _di_kt_remove_flag_mode_d_

/**
 * Defines for bitwise directory recurse flag enumeration combinations.
 *
 * _di_kt_remove_flag_recurse_*_d:
 *   - directory_not: All non-directory flags combined.
 */
#ifndef _di_kt_remove_flag_recurse_d_
  #define kt_remove_flag_recurse_directory_not_d (f_directory_recurse_do_flag_block_e | f_directory_recurse_do_flag_character_e | f_directory_recurse_do_flag_fifo_e | f_directory_recurse_do_flag_link_e | f_directory_recurse_do_flag_path_e | f_directory_recurse_do_flag_regular_e | f_directory_recurse_do_flag_socket_e | f_directory_recurse_do_flag_unknown_e)
#endif // _di_kt_remove_flag_recurse_d_

/**
 * Flags passed to the main function or program.
 *
 * kt_remove_main_flag_*_d:
 *   - none:                   No flags set.
 *   - accessed:               Remove by last accessed datetime.
 *   - block:                  Remove by file type: block.
 *   - character:              Remove by file type: character.
 *   - copyright:              Print copyright.
 *   - changed:                Remove by changed datetime.
 *   - different:              Remove by user different from caller.
 *   - directory:              Remove by file type: directory.
 *   - empty_all:              Helper flag representing all empty flag bits.
 *   - empty_only:             Remove empty directories.
 *   - empty_only_fail:        Fail on empty directories.
 *   - empty_not:              Remove not empty directories.
 *   - empty_not_fail:         Fail on not empty directories.
 *   - fifo:                   Remove by file type: FIFO.
 *   - follow:                 Follow symbolic links for deleting the file being pointed to rather than the link itself (when not set the link itself is deleted).
 *   - force:                  Forcibly delete.
 *   - group:                  Remove by GID.
 *   - help:                   Print help.
 *   - link:                   Remove by file type: link.
 *   - mode:                   Remove by mode.
 *   - option_used:            Designates that type match options are in use, overriding the default behavior.
 *   - prompt_all:             Helper flag representing all prompt flag bits.
 *   - prompt_each:            Operate in interactive mode, prompting for every file.
 *   - prompt_follow:          Operate in interactive mode: prompting for every link that is being followed.
 *   - prompt_never:           Do not operate in interactive mode.
 *   - prompt_once:            Operate in interactive mode: prompting if removing 3 or more files.
 *   - recurse:                Recurse directories.
 *   - regular:                Remove by file type: regular.
 *   - remember:               Enable remembering paths already processed.
 *   - same:                   Remove by same user as caller.
 *   - simulate:               Do not actually perform deletes, instead print messages (when silent, should still return 0 or 1).
 *   - socket:                 Remove by file type: socket.
 *   - tree:                   Remove directory tree (parent directories) (remove a/b/c, removes a/b/c, then a/b/, then a).
 *   - updated:                Remove by last updated datetime.
 *   - user:                   Remove by UID.
 *   - utc:                    Process dates in UTC mode.
 *   - version:                Print version.
 *   - version_copyright_help: A helper flag representing version, copyright, and help flag bits being set.
 */
#ifndef _di_kt_remove_main_flag_d_
  #define kt_remove_main_flag_none_d                   0x0
  #define kt_remove_main_flag_accessed_d               0x1
  #define kt_remove_main_flag_block_d                  0x2
  #define kt_remove_main_flag_character_d              0x4
  #define kt_remove_main_flag_copyright_d              0x8
  #define kt_remove_main_flag_changed_d                0x10
  #define kt_remove_main_flag_different_d              0x20
  #define kt_remove_main_flag_directory_d              0x40
  #define kt_remove_main_flag_empty_all_d              0x780
  #define kt_remove_main_flag_empty_only_d             0x80
  #define kt_remove_main_flag_empty_only_fail_d        0x100
  #define kt_remove_main_flag_empty_not_d              0x200
  #define kt_remove_main_flag_empty_not_fail_d         0x400
  #define kt_remove_main_flag_fifo_d                   0x800
  #define kt_remove_main_flag_follow_d                 0x1000
  #define kt_remove_main_flag_force_d                  0x2000
  #define kt_remove_main_flag_group_d                  0x4000
  #define kt_remove_main_flag_help_d                   0x8000
  #define kt_remove_main_flag_link_d                   0x10000
  #define kt_remove_main_flag_mode_d                   0x20000
  #define kt_remove_main_flag_option_used_d            0x40000
  #define kt_remove_main_flag_prompt_all_d             0x780000
  #define kt_remove_main_flag_prompt_each_d            0x80000
  #define kt_remove_main_flag_prompt_follow_d          0x100000
  #define kt_remove_main_flag_prompt_never_d           0x200000
  #define kt_remove_main_flag_prompt_once_d            0x400000
  #define kt_remove_main_flag_recurse_d                0x800000
  #define kt_remove_main_flag_regular_d                0x1000000
  #define kt_remove_main_flag_remember_d               0x2000000
  #define kt_remove_main_flag_same_d                   0x4000000
  #define kt_remove_main_flag_simulate_d               0x8000000
  #define kt_remove_main_flag_socket_d                 0x10000000
  #define kt_remove_main_flag_tree_d                   0x20000000
  #define kt_remove_main_flag_updated_d                0x40000000
  #define kt_remove_main_flag_user_d                   0x80000000
  #define kt_remove_main_flag_utc_d                    0x100000000
  #define kt_remove_main_flag_version_d                0x200000000
  #define kt_remove_main_flag_version_copyright_help_d 0x200008008
#endif // _di_kt_remove_main_flag_e_

/**
 * Flags for fine-tuned print control.
 *
 * kt_remove_print_flag_*_d:
 *   - none:    No flags set.
 *   - debug:   Stream is for debug printing.
 *   - error:   Stream is for error printing.
 *   - file:    Stream is associated with a file.
 *   - in:      Stream is a source file.
 *   - message: Stream is for message printing.
 *   - out:     Stream is a destination file.
 *   - warning: Stream is for warning printing.
 */
#ifndef _di_kt_remove_print_flag_d_
  #define kt_remove_print_flag_none_d    0x0
  #define kt_remove_print_flag_debug_d   0x1
  #define kt_remove_print_flag_error_d   0x2
  #define kt_remove_print_flag_file_d    0x4
  #define kt_remove_print_flag_in_d      0x8
  #define kt_remove_print_flag_message_d 0x10
  #define kt_remove_print_flag_out_d     0x20
  #define kt_remove_print_flag_warning_d 0x40
#endif // _di_kt_remove_print_flag_d_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_common_define_h
