#include "../remove.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifndef _di_kt_remove_cache_delete_
  void kt_remove_cache_delete(kt_remove_cache_t * const cache) {

    if (!cache) return;

    f_memory_array_resize(0, sizeof(f_char_t), (void **) &cache->buffer.string, &cache->buffer.used, &cache->buffer.size);

    f_memory_arrays_resize(0, sizeof(f_string_dynamic_t), (void **) &cache->files.array, &cache->files.used, &cache->files.size, &f_string_dynamics_delete_callback);
    f_memory_arrays_resize(0, sizeof(f_string_dynamic_t), (void **) &cache->memory.array, &cache->memory.used, &cache->memory.size, &f_string_dynamics_delete_callback);
    f_memory_arrays_resize(0, sizeof(f_string_dynamic_t), (void **) &cache->tree.array, &cache->tree.used, &cache->tree.size, &f_string_dynamics_delete_callback);
  }
#endif // _di_kt_remove_cache_delete_

#ifndef _di_kt_remove_main_delete_
  void kt_remove_main_delete(kt_remove_main_t * const main) {

    if (!main) return;

    fll_program_data_delete(&main->program);

    kt_remove_cache_delete(&main->cache);
    kt_remove_setting_delete(&main->setting);
  }
#endif // _di_kt_remove_main_delete_

#ifndef _di_kt_remove_setting_delete_
  void kt_remove_setting_delete(kt_remove_setting_t * const setting) {

    if (!setting) return;

    f_memory_arrays_resize(0, sizeof(f_string_dynamic_t), (void **) &setting->files.array, &setting->files.used, &setting->files.size, &f_string_dynamics_delete_callback);

    f_memory_array_resize(0, sizeof(kt_remove_date_t), (void **) &setting->accessed.array, &setting->accessed.used, &setting->accessed.size);
    f_memory_array_resize(0, sizeof(kt_remove_date_t), (void **) &setting->changed.array, &setting->changed.used, &setting->changed.size);
    f_memory_array_resize(0, sizeof(kt_remove_date_t), (void **) &setting->updated.array, &setting->updated.used, &setting->updated.size);
    f_memory_array_resize(0, sizeof(kt_remove_mode_t), (void **) &setting->modes.array, &setting->modes.used, &setting->modes.size);

    f_memory_array_resize(0, sizeof(f_gid_t), (void **) &setting->groups.array, &setting->groups.used, &setting->groups.size);
    f_memory_array_resize(0, sizeof(f_uid_t), (void **) &setting->users.array, &setting->users.used, &setting->users.size);
  }
#endif // _di_kt_remove_setting_delete_

#ifdef __cplusplus
} // extern "C"
#endif
