#include "remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_remove_operate_file_
  void kt_remove_operate_file(kt_remove_main_t * const main, const f_string_static_t path) {

    if (!main) return;

    if (!path.used) {
      main->setting.state.status = F_data_not;

      return;
    }

    if (kt_remove_signal_check(main)) return;

    const uint16_t flag_operate = kt_remove_preprocess_file(main, path, 0);

    if (F_status_is_error_not(main->setting.state.status) && !(main->setting.flag & kt_remove_main_flag_simulate_d) && !(flag_operate & kt_remove_flag_file_operate_processed_d)) {
      main->setting.state.status = flag_operate & kt_remove_flag_file_operate_directory_d
        ? kt_remove_operate_file_recurse(main, path, flag_operate)
        : kt_remove_operate_file_remove(main, path, flag_operate);
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      kt_remove_operate_memory_save(main, path, flag_operate);

      if (F_status_is_error(main->setting.state.status)) {
        kt_remove_print_error_file(&main->program.error, macro_kt_remove_f(kt_remove_operate_memory_save), path, f_file_operation_process_s, fll_error_file_type_path_e);
      }
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      main->setting.state.status = F_okay;
    }
  }
#endif // _di_kt_remove_operate_file_

#ifndef _di_kt_remove_operate_file_recurse_
  f_status_t kt_remove_operate_file_recurse(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate) {

    if (!kt_remove_operate_shall_remove(flag_operate)) return (flag_operate & kt_remove_flag_file_operate_remove_fail_d) ? F_status_set_error(F_no) : F_no;

    f_directory_recurse_do_t recurse = f_directory_recurse_do_t_initialize;

    recurse.state.custom = (void *) main;
    recurse.state.code = flag_operate;
    recurse.state.interrupt = &kt_remove_signal_check_recurse;

    recurse.flag = f_directory_recurse_do_flag_list_e;
    recurse.depth_max = kt_remove_depth_max_d;

    recurse.action = &kt_remove_operate_file_recurse_action;
    recurse.handle = &kt_remove_operate_file_recurse_handle;

    fl_directory_do(path, &recurse);

    const f_status_t status = f_directory_recurse_do_delete(&recurse);

    return F_status_is_error(recurse.state.status)
      ? recurse.state.status
      : F_status_is_error(status)
        ? status
        : F_yes;
  }
#endif // _di_kt_remove_operate_file_recurse_

#ifndef _di_kt_remove_operate_file_recurse_action_
  void kt_remove_operate_file_recurse_action(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag) {

    if (!recurse || !recurse->state.custom || F_status_set_fine(recurse->state.status) == F_interrupt) return;
    if (!kt_remove_operate_shall_remove(recurse->state.code) || !(flag & f_directory_recurse_do_flag_action_e)) return;

    recurse->state.status = kt_remove_operate_file_remove((kt_remove_main_t *) recurse->state.custom, recurse->path, recurse->state.code);
  }
#endif // _di_kt_remove_operate_file_recurse_action_

#ifndef _di_kt_remove_operate_file_recurse_handle_
  void kt_remove_operate_file_recurse_handle(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag) {

    if (!recurse || !recurse->state.custom || F_status_set_fine(recurse->state.status) == F_interrupt) return;

    kt_remove_main_t * const main = (kt_remove_main_t *) recurse->state.custom;

    // Arguments to fl_recurse_do() are invalid (parameter checking).
    if (flag == f_directory_recurse_do_flag_top_e) {
      kt_remove_print_error_status(&main->program.error, macro_kt_remove_f(fl_recurse_do), recurse->state.status);

      return;
    }

    // The top-level path is an empty string or an error occurred while processing the top-level path.
    if (flag == (f_directory_recurse_do_flag_top_e | f_directory_recurse_do_flag_path_e) || flag == (f_directory_recurse_do_flag_top_e | f_directory_recurse_do_flag_path_e | f_directory_recurse_do_flag_before_e)) {
      kt_remove_print_error_file_status(&main->program.error, macro_kt_remove_f(fl_recurse_do), name, f_file_operation_stat_s, fll_error_file_type_path_e, recurse->state.status);

      return;
    }

    // An error happened during directory list loading.
    if (flag == (f_directory_recurse_do_flag_list_e | f_directory_recurse_do_flag_path_e)) {
      kt_remove_print_error_file_status(&main->program.error, macro_kt_remove_f(fl_recurse_do), name, f_file_operation_list_s, fll_error_file_type_directory_e, recurse->state.status);

      return;
    }
  }
#endif // _di_kt_remove_operate_file_recurse_handle_

#ifndef _di_kt_remove_operate_file_remove_normal_
  f_status_t kt_remove_operate_file_remove(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate) {

    if (!main) return F_status_set_error(F_parameter);
    if (!kt_remove_operate_shall_remove(flag_operate)) return (flag_operate & kt_remove_flag_file_operate_remove_fail_d) ? F_status_set_error(F_no) : F_no;

    f_status_t status = F_no;

    if (flag_operate & kt_remove_flag_file_operate_follow_d) {
      main->cache.buffer.used = 0;

      status = f_file_link_read(path, F_false, &main->cache.buffer);

      if (F_status_is_error(status)) {
        kt_remove_print_error_file(&main->program.error, macro_kt_remove_f(f_file_remove), path, f_file_operation_stat_s, fll_error_file_type_link_e);

        return status;
      }
    }

    status = (flag_operate & f_directory_recurse_do_flag_directory_e)
      ? f_directory_remove((flag_operate & kt_remove_flag_file_operate_follow_d) ? main->cache.buffer : path, 0, F_false)
      : f_file_remove((flag_operate & kt_remove_flag_file_operate_follow_d) ? main->cache.buffer : path);

    if (F_status_is_error(status)) {
      kt_remove_print_error_file(&main->program.error, macro_kt_remove_f(f_file_remove), (flag_operate & kt_remove_flag_file_operate_follow_d) ? main->cache.buffer : path, f_file_operation_delete_s, fll_error_file_type_file_e);
    }
    else {
      status = F_yes;
    }

    return status;
  }
#endif // _di_kt_remove_operate_file_remove_normal_

#ifndef _di_kt_remove_operate_memory_check_
  void kt_remove_operate_memory_check(kt_remove_main_t * const main, const f_string_static_t path, uint16_t * const flag_operate) {

    if (!main) return;

    if (!path.used) {
      main->setting.state.status = F_data_not;

      return;
    }

    *flag_operate &= ~kt_remove_flag_file_operate_processed_d;

    main->setting.state.status = F_okay;

    if (!main->cache.memory.used) return;

    f_range_t range = f_range_t_initialize;
    f_number_unsigned_t i = 0;

    if (main->setting.flag & kt_remove_main_flag_recurse_d) {
      for (; i < main->cache.memory.used; ++i) {

        if (kt_remove_signal_check(main)) return;
        if (!main->cache.memory.array[i].used) continue;

        // The memory cache is nulless and top-most directories end in slashes.
        range.start = 0;
        range.stop = main->cache.memory.array[i].used - 1;

        if (main->cache.memory.array[i].string[range.stop] == f_path_separator_s.string[0]) {
          if (f_compare_dynamic_partial_dynamic(main->cache.memory.array[i], path, range) == F_equal_to) {
            *flag_operate |= kt_remove_flag_file_operate_processed_d;

            break;
          }

          // Perform exact match without the trailing directory slash.
          if (range.stop) {
            --range.stop;
          }

          if (f_compare_dynamic_partial_dynamic(main->cache.memory.array[i], path, range) == F_equal_to) {
            *flag_operate |= kt_remove_flag_file_operate_processed_d;

            break;
          }
        }
        else {
          if (f_compare_dynamic(main->cache.memory.array[i], path) == F_equal_to) {
            *flag_operate |= kt_remove_flag_file_operate_processed_d;

            break;
          }
        }
      } // for
    }
    else {

      // Only perform exact matches when not using recursion.
      for (; i < main->cache.memory.used; ++i) {

        if (kt_remove_signal_check(main)) return;

        if (f_compare_dynamic(main->cache.memory.array[i], path) == F_equal_to) {
          *flag_operate |= kt_remove_flag_file_operate_processed_d;

          break;
        }
      } // for
    }
  }
#endif // _di_kt_remove_operate_memory_check_

#ifndef _di_kt_remove_operate_memory_save_
  void kt_remove_operate_memory_save(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate) {

    if (!main) return;

    if (!path.used) {
      main->setting.state.status = F_data_not;

      return;
    }

    if (!(main->setting.flag & kt_remove_main_flag_remember_d)) {
      main->setting.state.status = F_okay;

      return;
    }

    main->setting.state.status = f_memory_array_increase(main->setting.state.step_small, sizeof(f_string_dynamic_t), (void **) &main->cache.memory.array, &main->cache.memory.used, &main->cache.memory.size);

    // Find any child paths that would be included by this and remove them from the paths list.
    if (F_status_is_error_not(main->setting.state.status) && (flag_operate & kt_remove_flag_file_operate_directory_d)) {
      main->cache.buffer.used = 0;

      main->setting.state.status = f_memory_array_increase_by(path.used + f_path_separator_s.used, sizeof(f_char_t), (void **) &main->cache.buffer.string, &main->cache.buffer.used, &main->cache.buffer.size);

      if (F_status_is_error_not(main->setting.state.status)) {
        main->setting.state.status = f_string_dynamic_append_nulless(path, &main->cache.buffer);
      }

      if (F_status_is_error_not(main->setting.state.status)) {
        main->setting.state.status = f_string_dynamic_append_nulless(f_path_separator_s, &main->cache.buffer);
      }

      if (F_status_is_error_not(main->setting.state.status)) {
        const f_range_t range = macro_f_range_t_initialize_2(main->cache.buffer.used);
        f_number_unsigned_t i = 0;

        for (; i < main->cache.memory.used; ++i) {

          if (kt_remove_signal_check(main)) return;
          if (main->cache.buffer.used > main->cache.memory.array[i].used) continue;

          if (f_compare_dynamic_partial_dynamic(main->cache.buffer, main->cache.memory.array[i], range) == F_equal_to) break;
        } // for

        // Swap the first and the last paths and then remove the last path.
        if (i < main->cache.memory.used) {
          const f_string_static_t temporary = macro_f_string_dynamic_t_initialize_1(main->cache.memory.array[i].string, main->cache.memory.array[i].size, main->cache.memory.array[i].used);

          main->cache.memory.array[i].string = main->cache.memory.array[main->cache.memory.used].string;
          main->cache.memory.array[i].size = main->cache.memory.array[main->cache.memory.used].size;
          main->cache.memory.array[i].used = main->cache.memory.array[main->cache.memory.used].used;

          main->cache.memory.array[main->cache.memory.used].string = temporary.string;
          main->cache.memory.array[main->cache.memory.used].size = temporary.size;
          main->cache.memory.array[main->cache.memory.used--].used = 0;
        }
      }
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      main->cache.memory.array[main->cache.memory.used].used = 0;

      if (flag_operate & kt_remove_flag_file_operate_directory_d) {
        main->setting.state.status = f_memory_array_increase_by(path.used + f_path_separator_s.used, sizeof(f_char_t), (void **) &main->cache.memory.array[main->cache.memory.used].string, &main->cache.memory.array[main->cache.memory.used].used, &main->cache.memory.array[main->cache.memory.used].size);
      }
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      main->setting.state.status = f_string_dynamic_append_nulless(path, &main->cache.memory.array[main->cache.memory.used]);
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      if (flag_operate & kt_remove_flag_file_operate_directory_d) {
        main->setting.state.status = f_string_dynamic_append_nulless(f_path_separator_s, &main->cache.memory.array[main->cache.memory.used]);
      }
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      ++main->cache.memory.used;
      main->setting.state.status = F_okay;
    }
  }
#endif // _di_kt_remove_operate_memory_save_

#ifndef _di_kt_remove_operate_shall_remove_
  f_status_t kt_remove_operate_shall_remove(const uint16_t flag) {

    return (flag & kt_remove_flag_file_operate_remove_d) && !(flag & kt_remove_flag_file_operate_remove_not_fail_d);
  }
#endif // _di_kt_remove_operate_shall_remove_

#ifdef __cplusplus
} // extern "C"
#endif
