#include "remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_remove_preprocess_file_
  uint16_t kt_remove_preprocess_file(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate) {

    if (!main) return 0;

    if (!path.used) {
      main->setting.state.status = F_data_not;

      return 0;
    }

    if (kt_remove_signal_check(main)) return 0;

    kt_remove_print_simulate_operate_file(&main->program.output, path, flag_operate);

    main->setting.state.status = f_file_exists(path, main->setting.flag & kt_remove_main_flag_follow_d);

    uint16_t flag_out = (main->setting.flag & kt_remove_main_flag_option_used_d) ? 0 : kt_remove_flag_file_operate_remove_d;

    if (main->setting.state.status == F_true) {
      const f_status_t status = f_file_is(path, F_file_type_link_d, F_false);

      if (F_status_is_error(status)) {
        main->setting.state.status = status;

        if (!(main->setting.flag & kt_remove_main_flag_simulate_d)) {
          remove_print_warning_file_reason(&main->program.warning, path, kt_remove_print_reason_stat_fail_s);
        }

        return 0;
      }

      if (status == F_true) {
        flag_out |= kt_remove_flag_file_operate_link_d;
      }
    }

    kt_remove_print_simulate_operate_file_exists(&main->program.output, path, flag_out);

    if (main->setting.state.status == F_false) {
      if (!(main->setting.flag & kt_remove_main_flag_simulate_d)) {
        remove_print_warning_file_reason(&main->program.warning, path, kt_remove_print_reason_not_found_s);
      }

      return 0;
    }

    if (F_status_is_error(main->setting.state.status)) {
      if (!(main->setting.flag & kt_remove_main_flag_simulate_d)) {
        remove_print_warning_file_reason(&main->program.warning, path, kt_remove_print_reason_no_access_s);
      }

      return 0;
    }

    if (main->setting.state.status == F_false) {
      kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_remove_s, F_false);

      return 0;
    }

    if (kt_remove_signal_check(main)) return 0;

    if (main->setting.flag & kt_remove_main_flag_follow_d) {
      flag_out |= kt_remove_flag_file_operate_follow_d;
    }

    f_number_unsigned_t i = 0;

    struct stat statistics;

    memset(&statistics, 0, sizeof(struct stat));

    main->setting.state.status = f_file_stat(path, main->setting.flag & kt_remove_main_flag_follow_d, &statistics);

    kt_remove_print_simulate_operate_file_stat(&main->program.output, statistics);

    if (F_status_is_error(main->setting.state.status)) {
      if (!(main->setting.flag & kt_remove_main_flag_simulate_d)) {
        remove_print_warning_file_reason(&main->program.warning, path, kt_remove_print_reason_stat_fail_s);
      }

      return flag_out;
    }

    if (main->setting.flag & kt_remove_main_flag_block_d) {
      if (macro_f_file_type_is_block(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_character_d) {
      if (macro_f_file_type_is_character(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (macro_f_file_type_is_directory(statistics.st_mode)) {
      flag_out |= kt_remove_flag_file_operate_directory_d;

      if (main->setting.flag & kt_remove_main_flag_directory_d) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_fifo_d) {
      if (macro_f_file_type_is_fifo(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_link_d) {
      if (macro_f_file_type_is_link(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_regular_d) {
      if (macro_f_file_type_is_regular(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_socket_d) {
      if (macro_f_file_type_is_socket(statistics.st_mode)) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_user_d) {
      for (i = 0; i < main->setting.users.used; ++i) {

        if (kt_remove_signal_check(main)) return flag_out;
        if (statistics.st_uid == (uid_t) main->setting.users.array[i]) break;
      } // for

      if (i < main->setting.users.used) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_same_d) {
      if (statistics.st_uid != geteuid()) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_different_d) {
      if (statistics.st_uid == geteuid()) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_group_d) {
      for (i = 0; i < main->setting.groups.used; ++i) {

        if (kt_remove_signal_check(main)) return flag_out;
        if (statistics.st_gid == (gid_t) main->setting.groups.array[i]) break;
      } // for

      if (i < main->setting.groups.used) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (main->setting.flag & kt_remove_main_flag_mode_d) {
      const mode_t mode = statistics.st_mode & F_file_mode_all_d;

      for (i = 0; i < main->setting.modes.used; ++i) {

        if (kt_remove_signal_check(main)) return flag_out;

        if (main->setting.modes.array[i].type == kt_remove_flag_mode_different_d) {
          if (main->setting.modes.array[i].mode & ~mode) break;
        }
        else if (main->setting.modes.array[i].type == kt_remove_flag_mode_same_d) {
          if (main->setting.modes.array[i].mode == mode) break;
        }
        else if (main->setting.modes.array[i].type == kt_remove_flag_mode_similar_d) {
          if (main->setting.modes.array[i].mode & mode) break;
        }
        else if (main->setting.modes.array[i].type == kt_remove_flag_mode_not_d) {
          if (main->setting.modes.array[i].mode != mode) break;
        }
      } // for

      if (i < main->setting.modes.used) {
        flag_out |= kt_remove_flag_file_operate_remove_d;
      }
    }

    if (flag_out & kt_remove_flag_file_operate_directory_d) {
      flag_out |= kt_remove_flag_file_operate_recurse_d;

      main->setting.state.status = f_directory_empty(path);
      if (F_status_is_error(main->setting.state.status)) return flag_out;

      if (main->setting.state.status) {
        flag_out |= kt_remove_flag_file_operate_empty_d;
      }

      if (main->setting.flag & kt_remove_main_flag_empty_all_d) {
        if (main->setting.state.status) {
          if (main->setting.flag & kt_remove_main_flag_empty_not_fail_d) {
            flag_out |= kt_remove_flag_file_operate_remove_fail_d;
          }
          else if (main->setting.flag & kt_remove_main_flag_empty_not_d) {
            flag_out |= kt_remove_flag_file_operate_remove_not_d;
          }
        }
        else {
          if (main->setting.flag & kt_remove_main_flag_empty_only_fail_d) {
            flag_out |= kt_remove_flag_file_operate_remove_fail_d;
          }
          else if (main->setting.flag & kt_remove_main_flag_empty_only_d || !(main->setting.flag & kt_remove_main_flag_recurse_d)) {
            flag_out |= kt_remove_flag_file_operate_remove_not_d;
          }
        }
      }
      else if (!main->setting.state.status) {
        if (!(main->setting.flag & kt_remove_main_flag_recurse_d)) {
          flag_out |= kt_remove_flag_file_operate_remove_not_d;
        }
      }
    }

    kt_remove_operate_memory_check(main, path, &flag_out);
    if (F_status_is_error(main->setting.state.status)) return flag_out;

    if (main->setting.flag & kt_remove_main_flag_force_d) {
      kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_force_s, F_true);

      flag_out |= kt_remove_flag_file_operate_remove_d;
    }

    if (flag_out & kt_remove_flag_file_operate_directory_d) {
      kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_recurse_s, (main->setting.flag & kt_remove_main_flag_recurse_d) && !(flag_operate & kt_remove_flag_file_operate_parent_d));
    }

    if (main->setting.flag & kt_remove_main_flag_prompt_all_d) {
      if (main->setting.flag & (kt_remove_main_flag_prompt_each_d | kt_remove_main_flag_prompt_never_d)) {
        kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_prompt_s, main->setting.flag & kt_remove_main_flag_prompt_each_d);
      }
      else if (main->setting.flag & kt_remove_main_flag_prompt_follow_d) {
        kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_prompt_s, (main->setting.flag & kt_remove_main_flag_follow_d) && (flag_out & kt_remove_flag_file_operate_link_d));
      }
      else {
        kt_remove_print_simulate_operate_prompt_once(&main->program.output, main->setting.files.used > 2 || main->setting.flag & kt_remove_main_flag_recurse_d);
      }
    }

    kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_remove_s, kt_remove_operate_shall_remove(flag_out));

    if (main->setting.flag & kt_remove_main_flag_remember_d) {
      kt_remove_print_simulate_operate_boolean(&main->program.output, kt_remove_skip_s, flag_out & kt_remove_flag_file_operate_processed_d);
    }

    if (main->call.process_operate_file_simulate) {
      main->setting.state.status = F_okay;

      main->call.process_operate_file_simulate(main, path, statistics, flag_operate, &flag_out);
      if (F_status_is_error(main->setting.state.status)) return flag_out;

      if (main->setting.state.status == F_done) {
        main->setting.state.status = F_okay;

        return flag_out;
      }
    }

    // At this point, the remove situation should be known so recurse into parent or child paths as appropriate before returning.
    if (flag_out & kt_remove_flag_file_operate_directory_d) {
      main->setting.state.status = F_okay;

      if (main->setting.flag & kt_remove_main_flag_tree_d) {
        f_range_t range = macro_f_range_t_initialize_2(path.used);

        if (range.stop > range.start) {
          main->setting.state.status = f_string_dynamic_seek_to_back(path, f_string_ascii_slash_forward_s.string[0], &range);

          if (F_status_is_error_not(main->setting.state.status) && F_status_set_fine(main->setting.state.status) == F_okay && range.stop > range.start) {
            --range.stop;

            for (i = 0; i < main->cache.tree.used; ++i) {
              if (f_compare_dynamic_partial_dynamic(main->cache.tree.array[i], path, range) == F_equal_to) break;
            } // for

            if (i == main->cache.tree.used) {
              main->setting.state.status = f_memory_array_increase(kt_remove_allocation_small_d, sizeof(f_string_dynamic_t), (void **) &main->cache.tree.array, &main->cache.tree.used, &main->cache.tree.size);

              if (F_status_is_error(main->setting.state.status)) {
                kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_memory_array_increase));

                return flag_out;
              }

              main->cache.tree.array[main->cache.tree.used].used = 0;

              main->setting.state.status = f_string_dynamic_partial_append(path, range, &main->cache.tree.array[main->cache.tree.used]);

              if (F_status_is_error(main->setting.state.status)) {
                kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_string_dynamic_partial_append));

                return flag_out;
              }

              ++main->cache.tree.used;

              f_print_dynamic(f_string_eol_s, main->program.output.to);

              kt_remove_preprocess_file(main, main->cache.tree.array[main->cache.tree.used - 1], kt_remove_flag_file_operate_parent_d);
            }
          }
          else if (F_status_is_error(main->setting.state.status)) {
            kt_remove_print_error_file(&main->program.error, macro_kt_remove_f(f_string_dynamic_seek_to_back), path, f_file_operation_process_s, fll_error_file_type_path_e);

            return flag_out;
          }
        }
      }

      if (main->setting.flag & kt_remove_main_flag_recurse_d && !(flag_operate & kt_remove_flag_file_operate_parent_d)) {
        main->setting.state.status = kt_remove_preprocess_file_recurse(main, path, flag_out | kt_remove_flag_file_operate_child_d);
      }
    }

    if (F_status_is_error_not(main->setting.state.status)) {
      main->setting.state.status = F_okay;
    }

    return flag_out;
  }
#endif // _di_kt_remove_preprocess_file_

#ifndef _di_kt_remove_preprocess_file_recurse_
  f_status_t kt_remove_preprocess_file_recurse(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate) {

    if (!kt_remove_operate_shall_remove(flag_operate)) return (flag_operate & kt_remove_flag_file_operate_remove_fail_d) ? F_status_set_error(F_no) : F_no;

    f_directory_recurse_do_t recurse = f_directory_recurse_do_t_initialize;

    recurse.state.custom = (void *) main;
    recurse.state.code = flag_operate;
    recurse.state.interrupt = &kt_remove_signal_check_recurse;

    recurse.flag = f_directory_recurse_do_flag_list_e;
    recurse.depth_max = kt_remove_depth_max_d;

    recurse.action = &kt_remove_preprocess_file_recurse_action;
    recurse.handle = &kt_remove_operate_file_recurse_handle;

    fl_directory_do(path, &recurse);

    const f_status_t status = f_directory_recurse_do_delete(&recurse);

    return F_status_is_error(recurse.state.status)
      ? recurse.state.status
      : F_status_is_error(status)
        ? status
        : F_yes;
  }
#endif // _di_kt_remove_preprocess_file_recurse_

#ifndef _di_kt_remove_preprocess_file_recurse_action_
  void kt_remove_preprocess_file_recurse_action(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag) {

    if (!recurse || !recurse->state.custom || F_status_set_fine(recurse->state.status) == F_interrupt) return;

    if (flag & f_directory_recurse_do_flag_action_e) {
      kt_remove_preprocess_file((kt_remove_main_t *) recurse->state.custom, recurse->path, recurse->state.code);
    }
  }
#endif // _di_kt_remove_preprocess_file_recurse_action_

#ifdef __cplusplus
} // extern "C"
#endif
