#include "remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_remove_setting_load_
  void kt_remove_setting_load(const f_console_arguments_t arguments, kt_remove_main_t * const main) {

    if (!main) return;

    main->setting.flag &= ~kt_remove_main_flag_option_used_d;

    main->setting.state.step_small = kt_remove_allocation_console_d;

    f_console_parameter_process(arguments, &main->program.parameters, &main->setting.state, 0);

    main->setting.state.step_small = kt_remove_allocation_small_d;

    if (F_status_is_error(main->setting.state.status)) {
      kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_console_parameter_process));

      return;
    }

    {
      f_uint16s_t choices = f_uint16s_t_initialize;

      // Identify and prioritize "color context" parameters.
      {
        uint16_t choices_array[3] = { kt_remove_parameter_no_color_e, kt_remove_parameter_light_e, kt_remove_parameter_dark_e };
        choices.array = choices_array;
        choices.used = 3;

        const uint8_t modes[3] = { f_color_mode_not_e, f_color_mode_light_e, f_color_mode_dark_e };

        main->setting.state.status = fll_program_parameter_process_context(choices, modes, F_true, &main->program);

        if (F_status_is_error(main->setting.state.status)) {
          kt_remove_print_error(&main->program.error, macro_kt_remove_f(fll_program_parameter_process_context));

          return;
        }
      }

      // Identify and prioritize "verbosity" parameters.
      {
        uint16_t choices_array[5] = { kt_remove_parameter_verbosity_quiet_e, kt_remove_parameter_verbosity_error_e, kt_remove_parameter_verbosity_verbose_e, kt_remove_parameter_verbosity_debug_e, kt_remove_parameter_verbosity_normal_e };
        choices.array = choices_array;
        choices.used = 5;

        const uint8_t verbosity[5] = { f_console_verbosity_quiet_e, f_console_verbosity_error_e, f_console_verbosity_verbose_e, f_console_verbosity_debug_e, f_console_verbosity_normal_e };

        main->setting.state.status = fll_program_parameter_process_verbosity(choices, verbosity, F_true, &main->program);

        if (F_status_is_error(main->setting.state.status)) {
          kt_remove_print_error(&main->program.error, macro_kt_remove_f(fll_program_parameter_process_verbosity));

          return;
        }
      }
    }

    f_number_unsigned_t i = 0;
    f_number_unsigned_t index = 0;
    f_number_unsigned_t index2 = 0;
    f_number_unsigned_t total_locations = 0;
    f_number_unsigned_t total_arguments = 0;

    uint8_t j = 0;

    main->setting.flag &= ~kt_remove_main_flag_version_copyright_help_d;

    if (main->program.parameters.array[kt_remove_parameter_help_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_help_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_version_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_version_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_copyright_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_copyright_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_block_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_block_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_character_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_character_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_different_e].result & f_console_result_found_e) {
      main->setting.flag &= ~kt_remove_main_flag_same_d;
      main->setting.flag |= kt_remove_main_flag_different_d | kt_remove_main_flag_option_used_d;

      if (main->program.parameters.array[kt_remove_parameter_same_e].result & f_console_result_found_e) {
        index = main->program.parameters.array[kt_remove_parameter_different_e].locations.used;
        index2 = main->program.parameters.array[kt_remove_parameter_same_e].locations.used;

        if (main->program.parameters.array[kt_remove_parameter_different_e].locations.array[index] < main->program.parameters.array[kt_remove_parameter_same_e].locations.array[index2]) {
          main->setting.flag &= ~kt_remove_main_flag_different_d;
          main->setting.flag |= kt_remove_main_flag_same_d;
        }
      }
    }
    else if (main->program.parameters.array[kt_remove_parameter_same_e].result & f_console_result_found_e) {
      main->setting.flag &= ~kt_remove_main_flag_different_d;
      main->setting.flag |= kt_remove_main_flag_same_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_fifo_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_fifo_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_follow_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_follow_d;

      if (main->program.parameters.array[kt_remove_parameter_stay_e].result & f_console_result_found_e) {
        index = main->program.parameters.array[kt_remove_parameter_follow_e].locations.used;
        index2 = main->program.parameters.array[kt_remove_parameter_stay_e].locations.used;

        if (main->program.parameters.array[kt_remove_parameter_follow_e].locations.array[index] < main->program.parameters.array[kt_remove_parameter_stay_e].locations.array[index2]) {
          main->setting.flag &= ~kt_remove_main_flag_follow_d;
        }
      }
    }
    else if (main->program.parameters.array[kt_remove_parameter_stay_e].result & f_console_result_found_e) {
      main->setting.flag &= ~kt_remove_main_flag_follow_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_directory_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_directory_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_force_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_force_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_link_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_link_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_recurse_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_recurse_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_regular_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_regular_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_simulate_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_simulate_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_socket_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_socket_d | kt_remove_main_flag_option_used_d;
    }

    if (main->program.parameters.array[kt_remove_parameter_tree_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_tree_d;
    }

    {
      f_console_parameter_t * const parameters[] = {
        &main->program.parameters.array[kt_remove_parameter_accessed_e],
        &main->program.parameters.array[kt_remove_parameter_changed_e],
        &main->program.parameters.array[kt_remove_parameter_updated_e],
      };

      kt_remove_dates_t * const dates[] = {
        &main->setting.accessed,
        &main->setting.changed,
        &main->setting.updated,
      };

      const f_string_static_t longs[] = {
        kt_remove_long_accessed_s,
        kt_remove_long_changed_s,
        kt_remove_long_updated_s,
      };

      for (uint8_t p = 0; p < 3; ++p) {

        if (!(parameters[p]->result & f_console_result_found_e)) continue;

        if (parameters[p]->result & f_console_result_value_e) {
          total_locations = parameters[p]->locations.used;
          total_arguments = parameters[p]->values.used;

          if (total_locations * 2 != total_arguments) {
            main->setting.state.status = F_status_set_error(F_parameter);

            kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, longs[p], 2);

            return;
          }

          if (dates[p]->used + total_locations > dates[p]->size) {
            main->setting.state.status = f_memory_array_resize(dates[p]->size + (total_locations - dates[p]->size), sizeof(kt_remove_date_t), (void **) &dates[p]->array, &dates[p]->used, &dates[p]->size);

            if (F_status_is_error(main->setting.state.status)) {
              kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_memory_array_resize));

              return;
            }
          }

          uint8_t enumerations[] = {
            kt_remove_flag_date_equal_d,
            kt_remove_flag_date_less_d,
            kt_remove_flag_date_less_equal_d,
            kt_remove_flag_date_more_d,
            kt_remove_flag_date_more_equal_d,
            kt_remove_flag_date_not_d,
            kt_remove_flag_date_equal_d,
            kt_remove_flag_date_less_d,
            kt_remove_flag_date_less_equal_d,
            kt_remove_flag_date_more_d,
            kt_remove_flag_date_more_equal_d,
            kt_remove_flag_date_not_d,
          };

          f_string_static_t strings[] = {
            kt_remove_date_symbol_equal_s,
            kt_remove_date_symbol_less_s,
            kt_remove_date_symbol_less_equal_s,
            kt_remove_date_symbol_more_s,
            kt_remove_date_symbol_more_equal_s,
            kt_remove_date_symbol_not_s,
            kt_remove_date_word_equal_s,
            kt_remove_date_word_less_s,
            kt_remove_date_word_less_equal_s,
            kt_remove_date_word_more_s,
            kt_remove_date_word_more_equal_s,
            kt_remove_date_word_not_s,
          };

          main->setting.state.status = F_known_not;

          {
            for (i = 0; i < total_arguments; i += 2) {

              index = parameters[p]->values.array[i];
              dates[p]->array[dates[p]->used].operation = 0;
              dates[p]->array[dates[p]->used].type = 0;

              for (j = 0; j < 12; ++j) {

                if (f_compare_dynamic(main->program.parameters.arguments.array[index], strings[j]) == F_equal_to) {
                  dates[p]->array[dates[p]->used].operation = enumerations[j];

                  index2 = parameters[p]->values.array[i + 1];

                  kt_remove_convert_date(main, main->program.parameters.arguments.array[index2], &dates[p]->array[dates[p]->used]);
                  if (F_status_is_error(main->setting.state.status)) return;

                  ++dates[p]->used;

                  break;
                }

                if (kt_remove_signal_check(main)) return;
              } // for

              if (j == 12) {
                main->setting.state.status = F_status_set_error(F_parameter);

                kt_remove_print_error_parameter_unknown_value(&main->program.error, f_console_symbol_long_normal_s, longs[p], main->program.parameters.arguments.array[index]);

                return;
              }
            } // for
          }

          main->setting.flag |= kt_remove_main_flag_option_used_d;
        }
        else {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, longs[p], 2);

          return;
        }
      } // for
    }

    if (main->program.parameters.array[kt_remove_parameter_empty_e].result & f_console_result_found_e) {
      if (main->program.parameters.array[kt_remove_parameter_empty_e].result & f_console_result_value_e) {
        total_arguments = main->program.parameters.array[kt_remove_parameter_empty_e].values.used;

        if (main->program.parameters.array[kt_remove_parameter_empty_e].locations.used != total_arguments) {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_empty_s, 1);

          return;
        }

        index = main->program.parameters.array[kt_remove_parameter_empty_e].values.array[total_arguments - 1];

        main->setting.flag &= ~kt_remove_main_flag_empty_all_d;
        main->setting.flag |= kt_remove_main_flag_option_used_d;

        if (f_compare_dynamic(kt_remove_not_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_empty_not_d;
        }
        else if (f_compare_dynamic(kt_remove_not_fail_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_empty_not_fail_d;
        }
        else if (f_compare_dynamic(kt_remove_only_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_empty_only_d;
        }
        else if (f_compare_dynamic(kt_remove_only_fail_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_empty_only_fail_d;
        }
        else {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_unknown_value(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_empty_s, main->program.parameters.arguments.array[index]);

          return;
        }
      }
      else {
        main->setting.state.status = F_status_set_error(F_parameter);

        kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_empty_s, 1);

        return;
      }
    }

    kt_remove_setting_load_id(main, &main->program.parameters.array[kt_remove_parameter_group_e], &main->setting.groups, kt_remove_long_group_s, kt_remove_main_flag_group_d);
    if (F_status_is_error(main->setting.state.status)) return;

    if (main->program.parameters.array[kt_remove_parameter_mode_e].result & f_console_result_found_e) {
      if (main->program.parameters.array[kt_remove_parameter_mode_e].result & f_console_result_value_e) {
        total_locations = main->program.parameters.array[kt_remove_parameter_mode_e].locations.used;
        total_arguments = main->program.parameters.array[kt_remove_parameter_mode_e].values.used;

        if (total_locations * 2 != total_arguments) {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_mode_s, 2);

          return;
        }

        if (main->setting.modes.used + total_locations > main->setting.modes.size) {
          main->setting.state.status = f_memory_array_resize(main->setting.modes.size + (total_locations - main->setting.modes.size), sizeof(kt_remove_mode_t), (void **) &main->setting.modes.array, &main->setting.modes.used, &main->setting.modes.size);

          if (F_status_is_error(main->setting.state.status)) {
            kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_memory_array_resize));

            return;
          }
        }

        const uint8_t enumerations[] = {
          kt_remove_flag_mode_different_d,
          kt_remove_flag_mode_same_d,
          kt_remove_flag_mode_similar_d,
          kt_remove_flag_mode_not_d,
          kt_remove_flag_mode_different_d,
          kt_remove_flag_mode_same_d,
          kt_remove_flag_mode_similar_d,
          kt_remove_flag_mode_not_d,
        };

        const f_string_static_t strings[] = {
          kt_remove_mode_symbol_different_s,
          kt_remove_mode_symbol_same_s,
          kt_remove_mode_symbol_similar_s,
          kt_remove_mode_symbol_not_s,
          kt_remove_mode_word_different_s,
          kt_remove_mode_word_same_s,
          kt_remove_mode_word_similar_s,
          kt_remove_mode_word_not_s,
        };

        for (i = 0; i < total_locations; i += 2) {

          index = main->program.parameters.array[kt_remove_parameter_mode_e].values.array[i];

          main->setting.modes.array[main->setting.modes.used].type = 0;

          for (j = 0; j < 8; ++j) {

            if (f_compare_dynamic(main->program.parameters.arguments.array[index], strings[j]) == F_equal_to) {
              main->setting.modes.array[main->setting.modes.used].type = enumerations[j];

              index2 = main->program.parameters.array[kt_remove_parameter_mode_e].values.array[i + 1];

              main->setting.modes.array[main->setting.modes.used].mode = kt_remove_get_mode(main, main->program.parameters.arguments.array[index2]);
              if (F_status_is_error(main->setting.state.status)) return;

              ++main->setting.modes.used;

              break;
            }
          } // for

          if (j == 8) {
            main->setting.state.status = F_status_set_error(F_parameter);

            kt_remove_print_error_parameter_unknown_value(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_mode_s, main->program.parameters.arguments.array[index]);

            return;
          }
        } // for

        main->setting.flag |= kt_remove_main_flag_mode_d | kt_remove_main_flag_option_used_d;
      }
      else {
        main->setting.state.status = F_status_set_error(F_parameter);

        kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_mode_s, 2);

        return;
      }
    }

    kt_remove_setting_load_id(main, &main->program.parameters.array[kt_remove_parameter_user_e], &main->setting.users, kt_remove_long_user_s, kt_remove_main_flag_user_d);
    if (F_status_is_error(main->setting.state.status)) return;

    if (main->program.parameters.array[kt_remove_parameter_prompt_e].result & f_console_result_found_e) {
      if (main->program.parameters.array[kt_remove_parameter_prompt_e].result & f_console_result_value_e) {
        total_arguments = main->program.parameters.array[kt_remove_parameter_prompt_e].values.used;

        if (main->program.parameters.array[kt_remove_parameter_prompt_e].locations.used != total_arguments) {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_prompt_s, 1);

          return;
        }

        index = main->program.parameters.array[kt_remove_parameter_prompt_e].values.array[total_arguments - 1];

        main->setting.flag &= ~kt_remove_main_flag_prompt_all_d;

        if (f_compare_dynamic(kt_remove_each_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_prompt_each_d;
        }
        else if (f_compare_dynamic(kt_remove_follow_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_prompt_follow_d;
        }
        else if (f_compare_dynamic(kt_remove_once_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_prompt_once_d;
        }
        else if (f_compare_dynamic(kt_remove_never_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_prompt_never_d;
        }
        else {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_unknown_value(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_prompt_s, main->program.parameters.arguments.array[index]);

          return;
        }
      }
      else {
        main->setting.state.status = F_status_set_error(F_parameter);

        kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_prompt_s, 1);

        return;
      }
    }

    if (main->program.parameters.array[kt_remove_parameter_remember_e].result & f_console_result_found_e) {
      if (main->program.parameters.array[kt_remove_parameter_remember_e].result & f_console_result_value_e) {
        total_arguments = main->program.parameters.array[kt_remove_parameter_remember_e].values.used;

        if (main->program.parameters.array[kt_remove_parameter_remember_e].locations.used != total_arguments) {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_yesno(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_remember_s);

          return;
        }

        index = main->program.parameters.array[kt_remove_parameter_remember_e].values.array[total_arguments - 1];

        if (f_compare_dynamic(kt_remove_yes_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag |= kt_remove_main_flag_remember_d;
        }
        else if (f_compare_dynamic(kt_remove_no_s, main->program.parameters.arguments.array[index]) == F_equal_to) {
          main->setting.flag &= ~kt_remove_main_flag_remember_d;
        }
        else {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_unknown_value(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_remember_s, main->program.parameters.arguments.array[index]);

          return;
        }
      }
      else {
        main->setting.state.status = F_status_set_error(F_parameter);

        kt_remove_print_error_parameter_missing_value_requires_yesno(&main->program.error, f_console_symbol_long_normal_s, kt_remove_long_remember_s);

        return;
      }
    }

    if (main->program.parameters.array[kt_remove_parameter_utc_e].result & f_console_result_found_e) {
      main->setting.flag |= kt_remove_main_flag_utc_d;

      // Use the right most parameter when both --utc and --local are passed.
      if (main->program.parameters.array[kt_remove_parameter_local_e].result & f_console_result_found_e) {
        index = main->program.parameters.array[kt_remove_parameter_local_e].locations.array[main->program.parameters.array[kt_remove_parameter_local_e].locations.used];
        index2 = main->program.parameters.array[kt_remove_parameter_utc_e].locations.array[main->program.parameters.array[kt_remove_parameter_utc_e].locations.used];

        if (index > index2) {
          main->setting.flag &= ~kt_remove_main_flag_utc_d;
        }
      }
    }
    else if (main->program.parameters.array[kt_remove_parameter_local_e].result & f_console_result_found_e) {
      main->setting.flag &= ~kt_remove_main_flag_utc_d;
    }

    // Load all remaining files as static strings (setting size to 0).
    if (main->program.parameters.remaining.used) {
      main->setting.state.status = f_memory_array_increase_by(main->program.parameters.remaining.used, sizeof(f_string_dynamic_t), (void **) &main->setting.files.array, &main->setting.files.used, &main->setting.files.size);

      if (F_status_is_error(main->setting.state.status)) {
        kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_memory_array_increase_by));

        return;
      }

      for (i = 0; i < main->program.parameters.remaining.used; ++i, ++main->setting.files.used) {

        index = main->program.parameters.remaining.array[i];

        main->setting.files.array[main->setting.files.used].used = 0;

        fl_path_clean(main->program.parameters.arguments.array[index], &main->setting.files.array[main->setting.files.used]);

        if (F_status_is_error(main->setting.state.status)) {
          kt_remove_print_error_file(&main->program.error, macro_kt_remove_f(fl_path_clean), main->program.parameters.arguments.array[index], f_file_operation_process_s, fll_error_file_type_path_e);

          return;
        }
      } // for
    }
  }
#endif // _di_kt_remove_setting_load_

#ifndef _di_kt_remove_setting_load_id_
  void kt_remove_setting_load_id(kt_remove_main_t * const main, f_console_parameter_t * const parameter, f_ids_t * const ids, const f_string_static_t name, const uint64_t flag) {

    if (!main) return;

    if (!parameter || !ids) {
      main->setting.state.status = F_status_set_error(F_parameter);

      kt_remove_print_error(&main->program.error, macro_kt_remove_f(kt_remove_setting_load));

      return;
    }

    if (parameter->result & f_console_result_found_e) {
      if (parameter->result & f_console_result_value_e) {
        const f_number_unsigned_t total_arguments = parameter->values.used;

        if (parameter->locations.used != total_arguments) {
          main->setting.state.status = F_status_set_error(F_parameter);

          kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, name, 1);

          return;
        }

        main->setting.state.status = f_memory_array_increase_by(total_arguments, sizeof(f_id_t), (void **) &ids->array, &ids->used, &ids->size);

        if (F_status_is_error(main->setting.state.status)) {
          kt_remove_print_error(&main->program.error, macro_kt_remove_f(f_memory_array_increase_by));

          return;
        }

        for (f_number_unsigned_t i = 0; i < total_arguments; ++i) {

          ids->array[ids->used] = kt_remove_get_id(main, main->program.parameters.arguments.array[parameter->values.array[i]], F_false);

          if (F_status_is_error(main->setting.state.status)) {
            kt_remove_print_error(&main->program.error, macro_kt_remove_f(kt_remove_get_id));

            return;
          }

          ++ids->used;
        } // for

        main->setting.flag |= flag | kt_remove_main_flag_option_used_d;
      }
      else {
        main->setting.state.status = F_status_set_error(F_parameter);

        kt_remove_print_error_parameter_missing_value_requires_amount(&main->program.error, f_console_symbol_long_normal_s, name, 1);

        return;
      }
    }

    main->setting.state.status = F_okay;
  }
#endif // _di_kt_remove_setting_load_id_

#ifdef __cplusplus
} // extern "C"
#endif
