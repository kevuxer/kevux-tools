/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 */
#ifndef _kt_remove_main_preprocess_h
#define _kt_remove_main_preprocess_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Perform pre-processing (including simulation) of the file operation.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This alters main.setting.state.status:
 *     F_okay on success.
 *     F_data_not on success but file is an empty string.
 *
 *     Errors (with error bit) from: f_file_link_read().
 *     Errors (with error bit) from: f_file_remove().
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 * @return
 *   The resulting flags determined by the pre-process.
 *
 * @see f_file_link_read()
 * @see f_file_remove()
 */
#ifndef _di_kt_remove_preprocess_file_
  extern uint16_t kt_remove_preprocess_file(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate);
#endif // _di_kt_remove_preprocess_file_

/**
 * Perform actual file removal for directory files.
 *
 * @param main
 *   The main program and settings data.
 *
 *   Must not be NULL.
 *
 *   This does not directly alter main.setting.state.status.
 * @param path
 *   The path to the file to operate on.
 * @param flag_operate
 *   The operate file specific flags from kt_remove_flag_file_operate_*_e.
 *
 * @return
 *   F_yes on success and file remove.
 *   F_no on success and file not removed.
 *   F_data_not on success but path is an empty string.
 *
 *   F_no (with error bit) on failure and file is not to be removed or cannot be removed.
 *   F_recurse (with error bit) on max recursion depth reached.
 *
 *   Errors (with error bit) from: f_directory_recurse_do_delete()
 *   Errors (with error bit) from: fl_directory_do()
 *
 * @see f_directory_recurse_do_delete()
 * @see f_file_remove()
 * @see fl_directory_do()
 */
#ifndef _di_kt_remove_preprocess_file_recurse_
  extern f_status_t kt_remove_preprocess_file_recurse(kt_remove_main_t * const main, const f_string_static_t path, const uint16_t flag_operate);
#endif // _di_kt_remove_preprocess_file_recurse_

/**
 * Perform directory recursion for a single file operation action.
 *
 * @param recurse
 *   The directory recurse data.
 *
 *   Must not be NULL.
 * @param name
 *   The name of the file or directory the action is being performed on.
 *   Does not have the parent directory path.
 *   May be empty at the top level.
 * @param flag
 *   A flag representing the particular directory action being performed.
 *
 * @see f_directory_remove()
 * @see fl_directory_do()
 */
#ifndef _di_kt_remove_preprocess_file_recurse_action_
  extern void kt_remove_preprocess_file_recurse_action(f_directory_recurse_do_t * const recurse, const f_string_static_t name, const uint16_t flag);
#endif // _di_kt_remove_preprocess_file_recurse_action_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_remove_main_preprocess_h
