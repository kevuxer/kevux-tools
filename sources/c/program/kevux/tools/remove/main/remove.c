#include "remove.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_remove_main_
  void kt_remove_main(kt_remove_main_t * const main) {

    if (!main || F_status_is_error(main->setting.state.status)) return;

    main->setting.state.status = F_okay;

    if (main->setting.flag & kt_remove_main_flag_version_copyright_help_d) {
      if (main->setting.flag & kt_remove_main_flag_help_d) {
        if (main->call.print_help) {
          main->call.print_help(&main->program.output, main->program.context);
        }
      }
      else if (main->setting.flag & kt_remove_main_flag_version_d) {
        fll_program_print_version(&main->program.message, kt_remove_program_version_s);
      }
      else if (main->setting.flag & kt_remove_main_flag_copyright_d) {
        fll_program_print_copyright(&main->program.message, fll_program_copyright_year_author_s);
      }

      if (main->program.signal_received) {
        fll_program_print_signal_received(&main->program.warning, main->program.signal_received);
      }

      return;
    }

    if (main->call.process_normal) {
      main->call.process_normal(main);
    }

    if (kt_remove_signal_check(main)) {
      fll_program_print_signal_received(&main->program.warning, main->program.signal_received);
    }
  }
#endif // _di_kt_remove_main_

#ifndef _di_kt_remove_process_normal_operate_
  void kt_remove_process_normal_operate(kt_remove_main_t * const main) {

    if (!main) return;

    if (!main->setting.files.used) {
      kt_remove_print_error_parameter_no_files(&main->program.error);

      return;
    }

    kt_remove_print_simulate_operate(&main->program.output);

    main->setting.state.status = F_okay;

    for (f_number_unsigned_t i = 0; i < main->setting.files.used; ++i) {

      kt_remove_operate_file(main, main->setting.files.array[i]);

      if ((main->setting.flag & kt_remove_main_flag_simulate_d) && i + 1 < main->setting.files.used && (F_status_is_error_not(main->setting.state.status) || F_status_set_fine(main->setting.state.status) == F_interrupt)) {
        f_print_dynamic(f_string_eol_s, main->program.output.to);
      }

      if (F_status_is_error(main->setting.state.status)) break;
    } // for
  }
#endif // _di_kt_remove_process_normal_operate_

#ifdef __cplusplus
} // extern "C"
#endif
