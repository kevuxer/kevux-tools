/**
 * Kevux Tools - TacoCat
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * This program provides the base include for the tacocat program.
 */
#ifndef _kt_tacocat_tacocat_tacocat_h
#define _kt_tacocat_tacocat_tacocat_h

// Tacocat includes.
#include <program/kevux/tools/tacocat/main/tacocat.h>
#include <program/kevux/tools/tacocat/tacocat/string.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_tacocat_tacocat_tacocat_h
