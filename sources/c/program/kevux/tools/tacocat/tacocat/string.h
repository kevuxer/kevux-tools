/**
 * Kevux Tools - TacoCat
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common string structures for the tacocat program.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_tacocat_tacocat_string_h
#define _kt_tacocat_tacocat_string_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The program name.
 */
#ifndef _di_kt_tacocat_program_name_s_
  #define KT_TACOCAT_program_name_s      "tacocat"
  #define KT_TACOCAT_program_name_long_s "TacocaT"

  #define KT_TACOCAT_program_name_s_length      7
  #define KT_TACOCAT_program_name_long_s_length 7
#endif // _di_kt_tacocat_program_name_s_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_tacocat_tacocat_string_h
