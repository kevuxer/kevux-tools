#include "tacocat.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_kt_tacocat_main_
  void kt_tacocat_main(kt_tacocat_main_t * const main) {

    if (!main) return;

    if (F_status_is_error(main->setting.state.status)) return;

    main->setting.state.status = F_okay;

    if (main->setting.flag & kt_tacocat_main_flag_version_copyright_help_d) {
      if (main->setting.flag & kt_tacocat_main_flag_help_d) {
        kt_tacocat_print_message_help(&main->program.output, main->program.context);
      }
      else if (main->setting.flag & kt_tacocat_main_flag_version_d) {
        fll_program_print_version(&main->program.message, kt_tacocat_program_version_s);
      }
      else if (main->setting.flag & kt_tacocat_main_flag_copyright_d) {
        fll_program_print_copyright(&main->program.message, fll_program_copyright_year_author_s);
      }

      if (main->program.signal_received) {
        fll_program_print_signal_received(&main->program.warning, main->program.signal_received);
      }

      return;
    }

    main->setting.state.status = f_random_seed(F_random_seed_flag_block_not_d);

    // Try again, but only once if blocked.
    if (main->setting.state.status == F_status_set_error(F_again)) {
      const f_time_spec_t time = { .tv_sec = kt_tacocat_startup_seed_delay_second_d, .tv_nsec = kt_tacocat_startup_seed_delay_nanosecond_d };

      f_time_sleep_spec(time, 0);

      main->setting.state.status = f_random_seed(F_random_seed_flag_block_not_d);
    }

    if (F_status_is_error(main->setting.state.status)) {

      // Fall back to the far less secure but better than nothing method of using time as the seed.
      f_random_seed_set((unsigned int) time(0));

      main->setting.state.status = F_okay;
    }

    kt_tacocat_process_main(main);

    if (main->program.signal_received) {
      fll_program_print_signal_received(&main->program.warning, main->program.signal_received);
    }
  }
#endif // _di_kt_tacocat_main_

#ifdef __cplusplus
} // extern "C"
#endif
