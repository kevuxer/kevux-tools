/**
 * Kevux Tools - TacocaT
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provides the common enumeration types.
 *
 * This is auto-included and should not need to be explicitly included.
 */
#ifndef _kt_tacocat_main_common_enumeration_h
#define _kt_tacocat_main_common_enumeration_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The main program parameters.
 */
#ifndef _di_kt_tacocat_parameter_e_
  enum {
    kt_tacocat_parameter_help_e,
    kt_tacocat_parameter_copyright_e,
    kt_tacocat_parameter_light_e,
    kt_tacocat_parameter_dark_e,
    kt_tacocat_parameter_no_color_e,
    kt_tacocat_parameter_verbosity_quiet_e,
    kt_tacocat_parameter_verbosity_error_e,
    kt_tacocat_parameter_verbosity_normal_e,
    kt_tacocat_parameter_verbosity_verbose_e,
    kt_tacocat_parameter_verbosity_debug_e,
    kt_tacocat_parameter_version_e,

    kt_tacocat_parameter_interval_e,
    kt_tacocat_parameter_max_buffer_e,
    kt_tacocat_parameter_receive_e,
    kt_tacocat_parameter_resolve_e,
    kt_tacocat_parameter_send_e,
  }; // enum

  #define kt_tacocat_console_parameter_t_initialize \
    { \
      macro_fll_program_console_parameter_standard_initialize, \
      \
      macro_f_console_parameter_t_initialize_3(kt_tacocat_short_interval_s, kt_tacocat_long_interval_s, 1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_tacocat_short_max_buffer_s, kt_tacocat_long_max_buffer_s, 1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_tacocat_short_receive_s, kt_tacocat_long_receive_s, 2, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_tacocat_short_resolve_s, kt_tacocat_long_resolve_s, 1, f_console_flag_normal_e), \
      macro_f_console_parameter_t_initialize_3(kt_tacocat_short_send_s, kt_tacocat_long_send_s, 2, f_console_flag_normal_e), \
    }

  #define kt_tacocat_parameter_total_d (f_console_parameter_state_type_total_d + 5)
#endif // _di_kt_tacocat_parameter_e_

/**
 * Individual socket-specific steps for receiving.
 *
 * kt_tacocat_socket_step_receive_*_e:
 *   - none:    No flags set.
 *   - control: Reading and processing the Control block and Size block.
 *   - packet:  Reading and processing the rest of the Packet block (the Header and Payload sections).
 *   - find:    Find the Header section and everything else up until the Payload section.
 *   - extract: Process the loaded data, extracting the Header and Signature sections.
 *   - check:   Check the loaded data, doing size checks, interpreting the status, and performing any signature checks.
 *   - write:   Save the loaded Payload block to the file (write to the file).
 *   - next:    Send "next" or "done" packet.
 *   - done:    Done processing file.
 */
#ifndef _di_kt_tacocat_socket_step_receive_e_
  enum {
    kt_tacocat_socket_step_receive_none_e = 0,
    kt_tacocat_socket_step_receive_control_e,
    kt_tacocat_socket_step_receive_packet_e,
    kt_tacocat_socket_step_receive_find_e,
    kt_tacocat_socket_step_receive_extract_e,
    kt_tacocat_socket_step_receive_check_e,
    kt_tacocat_socket_step_receive_write_e,
    kt_tacocat_socket_step_receive_next_e,
    kt_tacocat_socket_step_receive_done_e,
  }; // enum
#endif // _di_kt_tacocat_socket_step_receive_e_

/**
 * Individual socket-specific steps for receiving.
 *
 * kt_tacocat_socket_step_send_*_e:
 *   - none:     No flags set.
 *   - size:     Determine the file size.
 *   - header:   Build and buffer the header.
 *   - build:    Build the header information.
 *   - file:     Buffer the file.
 *   - check:    Additional checks before sending, such as re-checking header size.
 *   - encode:   Encode entire packet.
 *   - packet:   Send the packet.
 *   - response: Wait for a response packet from the opposite end.
 *   - done:     The entire Packet is sent.
 */
#ifndef _di_kt_tacocat_socket_step_send_e_
  enum {
    kt_tacocat_socket_step_send_none_e = 0,
    kt_tacocat_socket_step_send_size_e,
    kt_tacocat_socket_step_send_header_e,
    kt_tacocat_socket_step_send_build_e,
    kt_tacocat_socket_step_send_file_e,
    kt_tacocat_socket_step_send_check_e,
    kt_tacocat_socket_step_send_encode_e,
    kt_tacocat_socket_step_send_packet_e,
    kt_tacocat_socket_step_send_wait_e,
    kt_tacocat_socket_step_send_done_e,
  }; // enum
#endif // _di_kt_tacocat_socket_step_send_e_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _kt_tacocat_main_common_enumeration_h
