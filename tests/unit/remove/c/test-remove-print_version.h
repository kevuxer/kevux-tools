/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Test the --help parameter.
 */
#ifndef _TEST__KT_remove__print_version
#define _TEST__KT_remove__print_version

/**
 * Test that the ++version parameter works.
 */
extern void test__kt_remove__print_version__works(void **state);

#endif // _TEST__KT_remove__print_version
