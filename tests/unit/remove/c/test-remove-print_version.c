#include "test-remove.h"
#include "test-remove-print_version.h"

#include <program/kevux/tools/remove/remove/main.h>

#ifdef __cplusplus
extern "C" {
#endif

void test__kt_remove__print_version__works(void **state) {

  mock_unwrap = 0;

  {
    const f_string_t argv[] = { "mocked_main", "++version", 0 };

    mock_status = F_okay;

    will_return(__wrap_fll_program_print_version, F_test);

    const int result = kt_main_test__remove(2, argv, 0);

    assert_int_equal(result, 0);
    assert_int_equal(mock_status, F_test);
  }

  {
    const f_string_t argv[] = { "mocked_main", "+v", 0 };

    mock_status = F_okay;

    will_return(__wrap_fll_program_print_version, F_test);

    const int result = kt_main_test__remove(2, argv, 0);

    assert_int_equal(result, 0);
    assert_int_equal(mock_status, F_test);
  }
}

#ifdef __cplusplus
} // extern "C"
#endif
