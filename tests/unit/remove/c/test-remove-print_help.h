/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Test the --help parameter.
 */
#ifndef _TEST__KT_remove__print_help
#define _TEST__KT_remove__print_help

/**
 * Test that the --help parameter works.
 */
extern void test__kt_remove__print_help__works(void **state);

#endif // _TEST__KT_remove__print_help
