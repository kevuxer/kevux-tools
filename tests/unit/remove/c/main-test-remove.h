/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Provide a main function wrapper.
 */
#ifndef _MAIN_TEST__remove_h
#define _MAIN_TEST__remove_h

// Remove includes.
#include <program/kevux/tools/remove/main/remove.h>
#include <program/kevux/tools/remove/remove/string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Wrapped program entry point.
 *
 * @param argc
 *   The number of arguments.
 * @param argv
 *   The array of arguments.
 * @param envp
 *   The array of all environment variables on program start.
 *
 * @return
 *   0 on success.
 *   1 on error.
 */
extern int main_test__remove(const int argc, const f_string_t *argv, const f_string_t *envp);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _MAIN_TEST__remove_h
