#include "mock-remove.h"

#ifdef __cplusplus
extern "C" {
#endif

int mock_unwrap = 0;
f_status_t mock_status = 0;

f_status_t __wrap_f_file_exists(const f_string_static_t path, const bool dereference) {

  const bool failure = mock_type(bool);

  if (failure) return mock_type(f_status_t);

  return mock_type(f_status_t);
}

f_status_t __wrap_f_file_remove(const f_string_static_t path) {

  const bool failure = mock_type(bool);

  if (failure) return mock_type(f_status_t);

  return mock_type(f_status_t);
}

f_status_t __wrap_fll_program_print_version(fl_print_t * const print, const f_string_static_t version) {

  mock_status = mock_type(f_status_t);

  return mock_status;
}

f_status_t __wrap_kt_remove_print_message_help(fl_print_t * const print, const f_color_context_t context) {

  mock_status = mock_type(f_status_t);

  return mock_status;
}

#ifdef __cplusplus
} // extern "C"
#endif
