/**
 * Kevux Tools - Remove
 *
 * Project: Kevux Tools
 * API Version: 0.5
 * Licenses: lgpl-2.1-or-later
 *
 * Test the remove project.
 */
#ifndef _MOCK__remove_h
#define _MOCK__remove_h

// Libc includes.
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>

// cmocka includes.
#include <cmocka.h>

// Remove includes.
#include <program/kevux/tools/remove/main/remove.h>
#include <program/kevux/tools/remove/remove/string.h>

#ifdef __cplusplus
extern "C" {
#endif

const static int mock_errno_generic = 32767;

extern int mock_unwrap;
extern f_status_t mock_status;

extern f_status_t __wrap_f_file_exists(const f_string_static_t path, const bool dereference);
extern f_status_t __wrap_f_file_remove(const f_string_static_t path);

extern f_status_t __wrap_fll_program_print_version(fl_print_t * const print, const f_string_static_t version);

extern f_status_t __wrap_kt_remove_print_message_help(fl_print_t * const print, const f_color_context_t context);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _MOCK__remove_h
